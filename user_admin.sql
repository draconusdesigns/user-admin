-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2014 at 03:24 PM
-- Server version: 5.1.62-log
-- PHP Version: 5.3.15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `user_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE IF NOT EXISTS `applications` (
  `application_id` int(12) NOT NULL AUTO_INCREMENT,
  `application` varchar(200) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`application_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`application_id`, `application`, `description`) VALUES
(1, 'Lexi', 'Central User Lexicon of user roles and access levels.'),
(2, 'Walter', 'Project Time management and tracking.');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `location_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `location` varchar(200) CHARACTER SET latin1 NOT NULL,
  `description` text,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`location_id`, `location`, `description`) VALUES
(1, 'Framingham', 'Home Office'),
(2, 'Quill', ''),
(3, 'Canada', ''),
(4, 'Lincolnshire', ''),
(6, 'Remote', '');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(4) NOT NULL AUTO_INCREMENT,
  `role` varchar(200) NOT NULL,
  `description` text,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role`, `description`) VALUES
(1, 'Admin', ''),
(2, 'Manager', ''),
(3, 'AE', ''),
(4, 'Design', ''),
(5, 'Editor', ''),
(6, 'Copywriter', '');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `status_id` int(12) NOT NULL AUTO_INCREMENT,
  `status` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`status_id`, `status`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Leave of Absence');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `team_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `team` varchar(200) CHARACTER SET latin1 NOT NULL,
  `description` text,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`team_id`, `team`, `description`) VALUES
(1, 'Cross Channel Creative', ''),
(2, 'Account', ''),
(3, 'Agency Systems', ''),
(4, 'Shared Services', ''),
(5, 'Copywriting', ''),
(6, 'Leadership', ''),
(7, 'Interactive', ''),
(8, 'Print', ''),
(9, 'Contract Creative', ''),
(10, 'Media', ''),
(11, 'Brand', ''),
(12, 'Inventory Management', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `first_name` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `status_id` int(8) DEFAULT NULL,
  `status_begin_date` date DEFAULT NULL,
  `status_end_date` date DEFAULT NULL,
  `team_id` int(8) DEFAULT NULL,
  `location_id` int(8) DEFAULT NULL,
  `manager_id` int(8) DEFAULT NULL,
  `email` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(10) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(10) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1310 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `first_name`, `last_name`, `status_id`, `status_begin_date`, `status_end_date`, `team_id`, `location_id`, `manager_id`, `email`, `start_date`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(720, 'carrigaa', 'Abigail', 'Carrigan', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'abigail.carrigan@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(721, 'trainera', 'Alex', 'Trainer', 1, '0000-00-00', '0000-00-00', 1, 1, 796, 'alex.trainer@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(722, 'langa', 'Alexander', 'Lang', 1, '0000-00-00', '0000-00-00', 1, 1, 848, 'alexander.lang@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(723, 'sheaa', 'Alexis', 'Shea', 1, '0000-00-00', '0000-00-00', 1, 1, 728, 'alexis.shea@staples.com', '2012-08-15', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(724, 'fauchera', 'Amanda', 'Faucher', 2, '0000-00-00', '0000-00-00', 1, 1, 791, 'amanda.faucher@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(725, 'pouliota', 'Amy', 'Pouliot', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'amy.pouliot@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(726, 'canam001', 'Amy', 'Candela', 1, '0000-00-00', '0000-00-00', 5, 1, 796, 'amy.candela@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(727, 'vathamy', 'Amy', 'Vath', 1, '0000-00-00', '0000-00-00', 1, 1, 843, 'amy.vath@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(728, 'meolaand', 'Andrea', 'Meola', 1, '0000-00-00', '0000-00-00', 1, 1, 735, 'andrea.meola@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(729, 'morehoua', 'Andrea', 'Morehouse', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'andrea.morehouse@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(730, 'fordang', 'Angela', 'Ford', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'angela.ford@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(731, 'gruap001', 'April', 'Grudier', 2, '0000-00-00', '0000-00-00', 2, 1, NULL, 'april.grudier@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-06-25 11:25:34', 873),
(732, 'edmondsa', 'Ash', 'Edmonds', 2, '0000-00-00', '0000-00-00', 1, 1, 848, 'ash.edmonds@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(733, 'eldridgeb', 'Bette', 'Eldridge', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'bette.eldridge@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(734, 'deweyb', 'Brian', 'Dewey', 2, '0000-00-00', '0000-00-00', 4, 1, 844, 'brian.dewey@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(735, 'distefab', 'Brian', 'Distefano', 1, '0000-00-00', '0000-00-00', 6, 1, 843, 'brian.distefano@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-04-14 10:17:28', 745),
(736, 'hoffmanb', 'Brian', 'Hoffman', 2, '0000-00-00', '0000-00-00', 7, 1, 833, 'brian.hoffman@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(737, 'tannerb', 'Brian', 'Tanner', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'brian.tanner@staples.com', '2009-06-06', '2009-06-06 09:00:00', 752, '2014-01-24 14:09:18', 752),
(738, 'zeisseb', 'Brook', 'Zeisse', 1, '0000-00-00', '0000-00-00', 7, 1, 756, 'brook.zeisse@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(739, 'hallc', 'Calvin', 'Hall', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'calvin.hall@staples.com', '2009-06-06', '2009-06-06 09:00:00', 752, '2014-01-24 14:09:18', 752),
(740, 'schwarca', 'Carol', 'Schwartz', 2, '0000-00-00', '0000-00-00', 6, 1, 862, 'carol.schwartz@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(741, 'foxc', 'Chris', 'Fox', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'chris.fox@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(742, 'arrigoch', 'Christine', 'Arrigo', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'christine.arrigo@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(743, 'caldesc', 'Chris', 'Caldes', 2, '0000-00-00', '0000-00-00', 3, 1, 745, 'chris.caldes@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-04-11 17:34:07', 745),
(744, 'turanoc', 'Christine', 'Turano', 1, '0000-00-00', '0000-00-00', 8, 1, 815, 'christine.turano@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(745, 'lebretoc', 'Cindy', 'Lebretore', 1, '0000-00-00', '0000-00-00', 6, 1, 1135, 'cindy.lebretore@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-08-11 13:10:12', 1308),
(746, 'alvordc', 'Clinton', 'Alvord', 1, '0000-00-00', '0000-00-00', 4, 1, 825, 'clinton.alvord@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(747, 'zehringc', 'Cosima', 'Zehring', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'cosima.zehring@staples.com', '2009-06-06', '2009-06-06 09:00:00', 752, '2014-01-24 14:09:18', 752),
(748, 'bakerc', 'Courtney', 'Baker', 1, '0000-00-00', '0000-00-00', 2, 1, 1243, 'courtney.baker@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 16:02:19', 873),
(749, 'rondined', 'Daniel', 'Rondinelli', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'daniel.rondinelli@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(750, 'noeldave', 'Dave', 'Noel', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'dave.noel@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(751, 'greavesd', 'Deborah', 'Greaves', 2, '0000-00-00', '0000-00-00', 2, 1, NULL, 'deborah.greaves@staples.com', '2013-10-01', '2009-06-06 09:00:00', 745, '2014-02-06 17:26:23', 745),
(752, 'skeathd', 'Deborah', 'Skeath', 1, '0000-00-00', '0000-00-00', 4, 1, 825, 'deborah.skeath@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(753, 'cappuccd', 'Diane', 'Cappuccio', 1, '0000-00-00', '0000-00-00', 9, 1, 837, 'diane.cappuccio@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(754, 'sharmad', 'Dileep', 'Sharma', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'dileep.sharma@staples.com', '2009-06-06', '2009-06-06 09:00:00', 752, '2014-01-24 14:09:18', 752),
(755, 'murchdot', 'Dotti', 'Murch', 1, '0000-00-00', '0000-00-00', 8, 1, 815, 'dotti.murch@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(756, 'hidalgoe', 'Efren', 'Hidalgo', 1, '0000-00-00', '0000-00-00', 7, 1, 847, 'efren.hidalgo@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(757, 'densene', 'Elaine', 'Densen', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'elaine.densen@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(758, 'guntore', 'Eric', 'Guntor', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'eric.guntor@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(759, 'mcelanee', 'Erin', 'McElaney', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'erin.mcelaney@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(760, 'miler001', 'Erin', 'Miliard', 1, '0000-00-00', '0000-00-00', 1, 1, 843, 'erin.miliard@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(761, 'simarrig', 'Gabrielle', 'Simarrian', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'gabrielle.simarrian@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(762, 'goodmang', 'Georgiana', 'Goodman', 2, '0000-00-00', '0000-00-00', 2, 1, 781, 'georgiana.goodman@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(763, 'davidg', 'Gina', 'David', 1, '0000-00-00', '0000-00-00', 8, 1, 745, 'gina.david@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(764, 'davisg', 'Gina', 'Davis', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'gina.davis@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(765, 'mortillg', 'Grail', 'Mortillaro', 1, '0000-00-00', '0000-00-00', 1, 1, 777, 'girolamo.mortillaro@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(766, 'ponsgr', 'Greg', 'Pons', 2, '0000-00-00', '0000-00-00', 3, 1, 789, 'greg.pons@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(767, 'mastroph', 'Hilary', 'Mastropaul', 1, '0000-00-00', '0000-00-00', 2, 1, NULL, 'hilary.mastropaul@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-07-30 17:12:59', 873),
(768, 'davidsoi', 'Inna', 'Davidson', 1, '0000-00-00', '0000-00-00', 1, 1, 727, 'inna.davidson@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(769, 'mestelj', 'Jaclyn', 'Mestel', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'jaclyn.mestel@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-07-10 16:55:54', 873),
(770, 'kargj', 'Jake', 'Karg', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'jake.karg@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(771, 'curranj', 'James', 'Curran', 1, '0000-00-00', '0000-00-00', 9, 1, 837, 'james.curran@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(772, 'hallockj', 'James', 'Hallock', 2, '0000-00-00', '0000-00-00', 2, 1, 1243, 'james.hallock@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(773, 'scanloja', 'Jane', 'Scanlon', 1, '0000-00-00', '0000-00-00', 9, 1, 843, 'jane.scanlon@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-04-21 12:54:26', 745),
(774, 'bilskyj', 'Jason', 'Bilsky', 1, '0000-00-00', '0000-00-00', 4, 1, 809, 'jason.bilsky@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-07-10 16:51:32', 873),
(775, 'sugarja', 'Jason', 'Sugar', 1, '0000-00-00', '0000-00-00', 5, 1, 843, 'jason.sugar@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(776, 'mccarthj', 'Jeanette', 'McCarthy', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'jeanette.mccarthy@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-04-21 12:51:09', 745),
(777, 'sugarmaj', 'Jeff', 'Sugarman', 1, '0000-00-00', '0000-00-00', 1, 1, 843, 'jeff.sugarman@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(778, 'dockingj', 'Jeffrey', 'Docking', 1, '0000-00-00', '0000-00-00', 2, 1, 767, 'jeffrey.docking@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 16:02:19', 873),
(779, 'berje001', 'Jennifer', 'Bergman', 1, '0000-00-00', '0000-00-00', 7, 1, 756, 'jennifer.bergman@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-06-06 10:02:43', 873),
(780, 'brindije', 'Jennifer', 'Brindisi', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'jennifer.brindisi@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(781, 'dipje001', 'Jennifer', 'DiPietro', 1, '0000-00-00', '0000-00-00', 2, 1, 1233, 'jennifer.dipietro@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 16:05:14', 873),
(782, 'millettj', 'Jennifer', 'Millett', 2, '0000-00-00', '0000-00-00', 4, 1, 844, 'jennifer.millett@staples.com', '2009-06-06', '2009-06-06 09:00:00', 752, '2014-01-24 14:09:18', 752),
(783, 'pascuccj', 'Jennifer', 'Pascucci', 2, '0000-00-00', '0000-00-00', 5, 1, 735, 'jennifer.pascucci@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(784, 'segje001', 'Jennifer', 'Segill', 1, '0000-00-00', '0000-00-00', 2, 1, 1243, 'jennifer.segill@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 16:02:19', 873),
(785, 'tusje001', 'Jennifer', 'Tusia', 2, '0000-00-00', '0000-00-00', 7, 1, 879, 'jennifer.tusia@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-04-14 13:53:23', 745),
(786, 'godbeyje', 'Jessica', 'Godbey', 1, '0000-00-00', '0000-00-00', 1, 1, 837, 'jessica.godbey@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(787, 'bonje001', 'Jessica', 'Bonn', 1, '0000-00-00', '0000-00-00', 7, 1, 756, 'jessica.bonn@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-06-06 10:05:32', 873),
(788, 'wrotnowj', 'Jessica', 'Wrotnowski', 1, '0000-00-00', '0000-00-00', 1, 1, 796, 'jessica.wrotnowski@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(789, 'atwoodjo', 'Joe', 'Atwood', 2, '0000-00-00', '0000-00-00', 1, 1, 727, 'joe.atwood@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(790, 'wolfortj', 'John', 'Wolforth', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'john.wolforth@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(791, 'zarbaj', 'John', 'Zarba', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'john.zarba@staples.com', '2009-06-06', '2009-06-06 09:00:00', 752, '2014-01-24 14:09:18', 752),
(792, 'brooksjo', 'Jonathan', 'Brooks', 2, '0000-00-00', '0000-00-00', 5, 1, 735, 'jonathan.brooks@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(793, 'smedleyj', 'Jonathan', 'Smedley', 1, '0000-00-00', '0000-00-00', 1, 1, 876, 'jonathan.smedley@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(794, 'melinoj', 'Joseph', 'Melino', 2, '0000-00-00', '0000-00-00', 3, 1, 745, 'joseph.melino@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(795, 'deeju001', 'Julia', 'Deegler', 2, '0000-00-00', '0000-00-00', 2, 1, 1029, 'julia.deegler@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(796, 'genereuk', 'Karen', 'Genereux', 1, '0000-00-00', '0000-00-00', 1, 1, 843, 'karen.genereux@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(797, 'ohlsonka', 'Kathleen', 'Ohlson', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'kathleen.ohlson@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(798, 'neffk', 'Kelly', 'Neff', 1, '0000-00-00', '0000-00-00', 2, 1, 731, 'kelly.neff@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 16:02:19', 873),
(799, 'lowke001', 'Kerry', 'Lowe', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'kerry.lowe@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 16:23:48', 873),
(800, 'daglek', 'Kevin', 'Dagle', 1, '0000-00-00', '0000-00-00', 4, 1, 809, 'kevin.dagle@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-07-10 16:51:15', 873),
(801, 'harriski', 'Kim', 'Harris', 1, '0000-00-00', '0000-00-00', 4, 1, 825, 'kim.harris@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 16:23:52', 743),
(802, 'wongk', 'Kimberly', 'Wong', 2, '0000-00-00', '0000-00-00', 9, 1, 843, 'kimberly.wong@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(803, 'curleyk', 'Krista', 'Curley', 2, '0000-00-00', '0000-00-00', 9, 1, 843, 'krista.curley@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(804, 'andkr001', 'Kristine', 'Andrews', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'kristine.andrews@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(805, 'willa002', 'Laura', 'Wilkas', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'laura.wilkas@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(806, 'varleyl', 'Lauralee', 'Varley', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'lauralee.varley@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-07-10 16:50:15', 873),
(807, 'harli001', 'Lindsay', 'Hartley', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'lindsay.hartley@staples.com', '2009-06-06', '2009-06-06 09:00:00', 752, '2014-01-24 14:09:18', 752),
(808, 'sommerl', 'Lindsay', 'Sommer', 2, '0000-00-00', '0000-00-00', 2, 1, 754, 'lindsay.sommer@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(809, 'dallamol', 'Lisa', 'Dallamora', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'lisa.dallamora@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-07-10 16:48:50', 873),
(810, 'mendesl', 'Luis', 'Mendes', 2, '0000-00-00', '0000-00-00', 3, 1, 745, 'luis.mendes@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(811, 'siekmeil', 'Luke', 'Siekmeier', 1, '0000-00-00', '0000-00-00', 7, 1, 866, 'luke.siekmeier@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-02-12 13:48:39', 873),
(812, 'blackcm', 'Marcus', 'Clark', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'marcus.blackclark@staples.com', '2009-06-06', '2009-06-06 09:00:00', 752, '2014-01-24 14:09:18', 752),
(813, 'conma002', 'Marilyn', 'Connolly', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'marilyn.connolly2@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(814, 'connomar', 'Mark', 'Connolly', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'mark.connolly@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(815, 'connorsm', 'Mary', 'Connors', 1, '0000-00-00', '0000-00-00', 8, 1, 745, 'mary.connors@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(816, 'joubertm', 'Mary', 'Joubert', 2, '0000-00-00', '0000-00-00', 1, 1, 727, 'mary.joubert@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(817, 'gallantm', 'Matthew', 'Gallant', 1, '0000-00-00', '0000-00-00', 8, 1, 745, 'matthew.gallant@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(818, 'isgromat', 'Matthew', 'Isgro', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'matthew.isgro@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-07-10 16:54:32', 873),
(819, 'woodfinm', 'Melanie', 'Woodfin', 2, '0000-00-00', '0000-00-00', 8, 1, 745, 'melanie.woodfin@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(820, 'andersom', 'Melissa', 'Anderson', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'melissa.anderson@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(821, 'hellmanm', 'Melissa', 'Hellman', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'melissa.hellman@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(822, 'berendsm', 'Michael', 'Berendsen', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'michael.berendsen@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(823, 'lovejoym', 'Michael', 'Lovejoy', 2, '0000-00-00', '0000-00-00', 9, 1, NULL, 'michael.lovejoy@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(824, 'portasm', 'Michelina', 'Portas', 2, '0000-00-00', '0000-00-00', 9, 1, 802, 'michelina.portas@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(825, 'lafleurm', 'Michelle', 'LaFleur', 1, '0000-00-00', '0000-00-00', 6, 1, 745, 'michelle.lafleur@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-04-14 10:18:40', 745),
(826, 'cinamonn', 'Nancy', 'Cinamon-Murray', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'nancy.cinamon-murray@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(827, 'nowickin', 'Noel', 'Nowicki', 1, '0000-00-00', '0000-00-00', 1, 1, 727, 'noel.nowicki@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 16:07:29', 873),
(828, 'sargentp', 'Paige', 'Sargent', 2, '0000-00-00', '0000-00-00', 6, 1, 1135, 'paige.sargent@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(829, 'ouipa001', 'Pamela', 'Ouimet', 1, '0000-00-00', '0000-00-00', 5, 1, 735, 'pamela.ouimet@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(830, 'simonp', 'Peter', 'Simon', 2, '0000-00-00', '0000-00-00', 6, 1, 862, 'peter.simon@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(831, 'inglesro', 'Robert', 'Ingles', 1, '0000-00-00', '0000-00-00', 4, 1, 825, 'robert.ingles@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(832, 'burnier', 'Robin', 'Burnie', 2, '0000-00-00', '0000-00-00', 2, 1, 828, '@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-05-19 11:07:59', 745),
(833, 'merro001', 'Robin', 'Merrill', 2, '0000-00-00', '0000-00-00', 7, 1, 843, 'robin.merrill@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(834, 'tremblas', 'Samantha', 'Tremblay', 2, '0000-00-00', '0000-00-00', 2, 1, NULL, 'samantha.tremblay@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(835, 'doxeys', 'Scott', 'Doxey', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'scott.doxey@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(836, 'wolfmans', 'Seth', 'Wolfman', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'seth.wolfman@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(837, 'resendes', 'Sheila', 'Resendes', 1, '0000-00-00', '0000-00-00', 1, 1, 773, 'sheila.resendes@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(838, 'beaudres', 'Stacey', 'Beaudreau', 2, '0000-00-00', '0000-00-00', 4, 1, 844, 'stacey.beaudreau@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(840, 'andwoods', 'Stefanie', 'Andwood', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'stefanie.andwood@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(841, 'klings', 'Stephanie', 'Kling', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'stephanie.kling@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(842, 'strattos', 'Susan', 'Stratton', 2, '0000-00-00', '0000-00-00', 9, 1, 796, 'susan.stratton@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(843, 'herdte', 'Teresa', 'Herd', 1, '0000-00-00', '0000-00-00', 6, 1, 1135, 'teresa.herd@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(844, 'sullivte', 'Terrence', 'Sullivan', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'terrence.sullivan@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-03-05 16:43:39', 745),
(845, 'corbettt', 'Thomas', 'Corbett', 2, '0000-00-00', '0000-00-00', 5, 1, 843, 'thomas.corbett@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(846, 'smitht', 'Tim', 'Smith', 2, '0000-00-00', '0000-00-00', 1, 1, 777, 'tim.smith@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(847, 'tobint', 'Tom', 'Tobin', 1, '0000-00-00', '0000-00-00', 6, 1, 843, 'tom.tobin@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(848, 'dettoret', 'Tony', 'Dettore', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'tony.dettore@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(849, 'vaccarit', 'Tony', 'Vaccarino', 2, '0000-00-00', '0000-00-00', 5, 1, 843, 'tony.vaccarino@staples.com', '2009-06-06', '2009-06-06 09:00:00', 743, '2014-01-24 14:09:18', 743),
(850, 'denarow', 'Wesley', 'Denaro', 1, '0000-00-00', '0000-00-00', 7, 1, 847, 'wesley.denaro@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(851, 'jacommew', 'Will', 'Jacomme', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'will.jacomme@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(855, 'gillisj', 'Jeff', 'Gillis', 1, '0000-00-00', '0000-00-00', 2, 1, 767, 'jeff.gillis@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-06-13 15:18:28', 873),
(856, 'watkinsk', 'Kate', 'Watkins', 1, '0000-00-00', '0000-00-00', 2, 1, 781, 'kate.watkins@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 16:02:19', 873),
(857, 'clearym', 'Mike', 'Cleary', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'mike.cleary@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(858, 'macba001', 'Barbara', 'Mackin', 2, '0000-00-00', '0000-00-00', 2, 1, NULL, 'barbara.mackin@staples.com', '2013-02-04', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(859, 'cepme001', 'Melanie', 'Cepeda', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'melanie.cepeda@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(860, 'ormesm', 'Michelle', 'Ormes', 2, '0000-00-00', '0000-00-00', 6, 1, 862, 'michelle.ormes@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(861, 'shawm', 'Marjorie', 'Shaw', 2, '0000-00-00', '0000-00-00', 6, 1, 862, 'marjorie.shaw@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(862, 'trueblor', 'Rachel', 'Trueblood', 2, '0000-00-00', '0000-00-00', 6, 1, NULL, 'rachel.trueblood@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(863, 'spraguga', 'Galen', 'Sprague', 2, '0000-00-00', '0000-00-00', 3, 1, NULL, 'galen.sprague@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(864, 'wesleyk', 'Kerry', 'Wesley', 2, '0000-00-00', '0000-00-00', 3, 1, NULL, 'kerry.wesley@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(866, 'mangiafw', 'Whitney', 'Mangiafico', 1, '0000-00-00', '0000-00-00', 5, 1, 773, 'whitney.mangiafico@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-02-12 13:31:00', 873),
(867, 'pecorelj', 'Joe', 'Pecorella', 1, '0000-00-00', '0000-00-00', 4, 1, 809, 'joseph.pecorella@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-07-10 16:50:58', 873),
(868, 'nicly001', 'Lyndsey', 'Nickerson', 1, '0000-00-00', '0000-00-00', 2, 1, 731, 'lyndsey.nickerson@staples.com', '2013-11-18', '2009-06-06 09:00:00', 873, '2014-01-24 16:02:19', 873),
(869, 'sankr001', 'Kristine', 'Sanchez', 2, '0000-00-00', '0000-00-00', 1, 1, 791, 'kristine.sanchez@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(870, 'phaja001', 'Jade', 'Phame', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'jade.phame@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(871, 'halma001', 'Matthew', 'Haley', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'matthew.haley@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(872, 'jewettg', 'Geoff', 'Jewett', 1, NULL, NULL, 3, 1, 1308, 'geoff.jewett@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-08-15 14:37:33', 872),
(873, 'wooda001', 'Dani', 'Woodman', 1, '0000-00-00', '0000-00-00', 3, 1, 1119, 'dani.woodman@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-08-11 13:09:17', 1308),
(874, 'crola001', 'Lauren', 'Croteau', 2, '0000-00-00', '0000-00-00', 5, 1, 735, 'lauren.croteau@staples.com', '2009-06-06', '2009-06-06 09:00:00', 752, '2014-01-24 14:09:18', 752),
(875, 'hamburgl', 'LuisFernando', 'Hamburger', 2, '0000-00-00', '0000-00-00', 4, 1, 844, 'luisfernando.hamburger@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(876, 'hamiltos', 'Scot', 'Hamilton', 1, '0000-00-00', '0000-00-00', 1, 1, 848, 'scott.hamilton@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(877, 'strla002', 'Laura', 'Strauss', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'laura.strauss@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(878, 'bagch001', 'Cheryl', 'Bagley', 2, '0000-00-00', '0000-00-00', 10, 1, 861, 'cheryl.bagley@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(879, 'tolma001', 'Mats', 'Tolander', 1, '0000-00-00', '0000-00-00', 7, 1, 847, 'mats.tolander@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(880, 'mtenney', 'Mark', 'Tenney', 2, '0000-00-00', '0000-00-00', 9, 1, 830, 'mark.tenney@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(881, 'nenginee', 'Nicole', 'Engineer', 2, '0000-00-00', '0000-00-00', 9, 1, 880, 'nicole.engineer@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(882, 'twesterg', 'Thom', 'Westergren', 2, '0000-00-00', '0000-00-00', 9, 1, 880, 'thom.westergren@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(883, 'agosteni', 'Adam', 'Gostenik', 1, '0000-00-00', '0000-00-00', 7, 1, 866, 'adam.gostenik@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-02-12 13:46:57', 873),
(884, 'kmolinel', 'Kim', 'Molinelli', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'kim.molinelli@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(885, 'alhopkin', 'Alison', 'Tedder', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'alison.hopkins@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(886, 'pramos', 'Paul', 'Ramos', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'paul.ramos@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(887, 'kbren', 'Kody', 'Bren', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'kody.bren@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(888, 'ababcock', 'Angie', 'Babcock', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'angie.babcock@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(889, 'rchurgov', 'Ray', 'Churgovich', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'ray.churgovich@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(890, 'efrazzel', 'Ted', 'Frazzell', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'ted.frazzell@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(891, 'jgruber', 'Joan', 'Gruber', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'joan.gruber@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(892, 'kkoersel', 'Kristie', 'Koerselman', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'kristie.koerselman@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(893, 'limarque', 'Linda', 'Marquez', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'Linda.Marquez@Staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(894, 'jnoffsin', 'Jason', 'Noffsinger', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'jason.noffsinger@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(895, 'mosboe', 'Megan', 'Osboe', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'megan.osboe@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(896, 'kweirich', 'Kimberly', 'Weirich', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'kimberly.weirich@staples.com', '2009-06-06', '2009-06-06 09:00:00', 794, '2014-01-24 14:09:18', 794),
(897, 'berli001', 'Lindsay', 'Yanklowski', 2, '0000-00-00', '0000-00-00', 7, 1, 879, 'lindsay.yanklowski@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-04-14 09:51:15', 745),
(898, 'ralickit', 'Tena', 'Ralicki', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'tena.ralicki@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(899, 'kearneym', 'Michael', 'Kearney', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'michael.kearney@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(900, 'zilkovav', 'Viera', 'Zilkova', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'viera.zilkova@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-07-10 16:55:29', 873),
(901, 'chaja001', 'Jamie', 'Chan', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'jamie.chan@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(902, 'danst001', 'Stanley', 'Dankoski', 1, '0000-00-00', '0000-00-00', 7, 1, 879, 'stanley.dankoski@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(903, 'robbinsj', 'Julie', 'Robbins', 1, '0000-00-00', '0000-00-00', 5, 1, 796, 'julie.robbins@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(904, 'bruje001', 'Jennifer', 'Bruno', 2, '0000-00-00', '0000-00-00', 1, 1, 796, 'jen.bruno@staples.com', '2013-08-05', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(905, 'hargron', 'Nicole', 'Hargrove', 1, '0000-00-00', '0000-00-00', 1, 1, 796, 'nicole.hargrove@staples.com', '2009-06-06', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(906, 'vidmi001', 'Mikala', 'Vidal', 2, '0000-00-00', '0000-00-00', 2, 1, 832, 'mikala.vidal@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(907, 'wilau001', 'Autumn', 'Wilbur', 1, '0000-00-00', '0000-00-00', 7, 1, 756, 'autumn.wilbur@staples.com', '2009-07-01', '2009-06-06 09:00:00', 873, '2014-01-24 14:09:18', 873),
(908, 'volel001', 'Ellie', 'Volckhausen', 2, '0000-00-00', '0000-00-00', 1, 1, 791, 'ellie.volckhausen@staples.com', '2009-06-06', '2009-06-06 09:00:00', 745, '2014-01-24 14:09:18', 745),
(909, 'stoda001', 'David', 'Stoker', 2, '0000-00-00', '0000-00-00', 1, 1, NULL, 'david.stoker@staples.com', '2009-06-06', '2009-07-02 16:41:24', 745, '2014-01-24 14:09:18', 745),
(910, 'kinki001', 'Kim', 'King', 2, '0000-00-00', '0000-00-00', 1, 1, 777, 'kim.king@staples.com', '2009-06-06', '2009-07-21 16:36:08', 745, '2014-01-24 14:09:18', 745),
(913, 'gilda001', 'Dan', 'Gilman', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'dan.gilman@staples.com', '2009-08-29', '2009-08-25 12:58:35', 745, '2014-01-24 14:09:18', 745),
(914, 'angan001', 'Anthony', 'Angotto', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'anthony.angotto@staples.com', '2009-10-23', '2009-10-23 10:59:13', 745, '2014-01-24 14:09:18', 745),
(915, 'colfr001', 'Francisco', 'Colom', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'francisco.colom@staples.com', '2009-10-28', '2009-10-28 14:32:06', 745, '2014-01-24 14:09:18', 745),
(916, 'rocla001', 'Lauren', 'Rochon', 2, '0000-00-00', '0000-00-00', 1, 1, 791, 'lauren.rochon@staples.com', '2010-01-09', '2010-01-08 12:47:03', 752, '2014-01-24 14:09:18', 752),
(917, 'cruda001', 'Dale', 'Cruse', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'dale.cruse@staples.com', '2010-01-16', '2010-01-11 16:39:46', 745, '2014-01-24 14:09:18', 745),
(918, 'sougl001', 'Glenn', 'Soulia', 2, '0000-00-00', '0000-00-00', 1, 1, 777, 'glenn.soulia@staples.com', '2010-01-16', '2010-01-14 16:37:38', 745, '2014-01-24 14:09:18', 745),
(919, 'kravi001', 'Victoria', 'Krasnakevick', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'victoria.krasnakevick@staples.com', '2010-01-23', '2010-01-19 16:43:22', 745, '2014-01-24 14:09:18', 745),
(920, 'walma001', 'Mark', 'Walker', 2, '0000-00-00', '0000-00-00', 5, 1, 843, 'mark.walker@staples.com', '2010-01-23', '2010-01-21 14:40:36', 873, '2014-01-24 14:09:18', 873),
(921, 'phidr001', 'Drew', 'Phillips', 1, '0000-00-00', '0000-00-00', 1, 1, 866, 'drew.phillips@staples.com', '2010-02-06', '2010-02-01 13:13:34', 873, '2014-02-12 13:47:34', 873),
(922, 'kelli001', 'Lisamarie', 'Kelly', 2, '0000-00-00', '0000-00-00', 4, 1, 844, 'lisamarie.kelly@staples.com', '2010-02-06', '2010-02-02 09:11:12', 745, '2014-01-24 14:09:18', 745),
(923, 'novju001', 'Justin', 'Novick', 1, '0000-00-00', '0000-00-00', 1, 1, 777, 'justin.novick@staples.com', '2010-02-13', '2010-02-08 15:04:24', 873, '2014-01-24 14:09:18', 873),
(924, 'berer001', 'Eric', 'Bertelsen', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'eric.bertelsen@staples.com', '2010-02-13', '2010-02-10 13:10:11', 752, '2014-01-24 14:09:18', 752),
(925, 'refra001', 'Rand', 'Refrigeri', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'rand.refrigeri@staples.com', '2010-02-13', '2010-02-10 13:28:16', 745, '2014-01-24 14:09:18', 745),
(926, 'bisci001', 'Cindy', 'Bishop', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'cindy.bishop@staples.com', '2011-06-09', '2010-02-18 11:00:06', 752, '2014-01-24 14:09:18', 752),
(927, 'lawan001', 'Andrew', 'Lawton', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'andrew.lawton@staples.com', '2010-02-20', '2010-02-18 11:01:48', 745, '2014-01-24 14:09:18', 745),
(928, 'mcmtr001', 'Tricia', 'McMahon', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'tricia.mcmahon@staples.com', '2010-02-27', '2010-02-23 11:01:05', 745, '2014-01-24 14:09:18', 745),
(929, 'pacma001', 'Marie', 'Pacelli', 1, '0000-00-00', '0000-00-00', 1, 1, 876, 'Marie.Pacelli@Staples.com', '2010-03-09', '2010-03-09 12:29:39', 873, '2014-01-24 14:09:18', 873),
(930, 'siest001', 'Stuart', 'Siegel', 1, '0000-00-00', '0000-00-00', 1, 1, 777, 'stuart.siegal@staples.com', '2010-03-20', '2010-03-15 14:42:23', 873, '2014-07-03 12:07:55', 873),
(931, 'farme001', 'Meghan', 'Farrar', 2, '0000-00-00', '0000-00-00', 4, 1, 844, 'meghan.farrar@staples.com', '2010-03-20', '2010-03-16 15:33:01', 873, '2014-01-24 14:09:18', 873),
(932, 'marpa002', 'Pamela', 'Martin', 2, '0000-00-00', '0000-00-00', 2, 1, 731, 'pamela.martin@staples.com', '2010-03-27', '2010-03-23 10:50:34', 745, '2014-01-24 14:09:18', 745),
(933, 'warma001', 'Maria', 'Warner', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'maria.warner@staples.com', '2010-04-03', '2010-03-30 13:59:41', 745, '2014-01-24 14:09:18', 745),
(934, 'velro002', 'Roxane', 'Velozo', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'roxane.velozo@staples.com', '2010-04-03', '2010-03-31 13:55:59', 745, '2014-01-24 14:09:18', 745),
(935, 'pecrh001', 'Rhonda', 'Peck', 2, '0000-00-00', '0000-00-00', 7, 1, 843, 'rhonda.peck@staples.com', '2011-05-18', '2010-03-31 13:58:32', 745, '2014-04-21 12:56:03', 745),
(936, 'murphyka', 'Kate', 'Murphy', 1, '0000-00-00', '0000-00-00', 2, 1, 1243, 'kate.murphy@staples.com', '2010-04-24', '2010-04-19 10:57:13', 873, '2014-01-24 16:02:19', 873),
(937, 'lepje001', 'Jennifer', 'LePore', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'jennifer.lepore@staples.com', '2010-05-15', '2010-05-11 12:13:54', 745, '2014-01-24 14:09:18', 745),
(938, 'cadde001', 'Deborah', 'Carty', 2, '0000-00-00', '0000-00-00', 5, 1, 728, 'deborah.carty@staples.com', '2010-05-22', '2010-05-18 17:17:09', 745, '2014-04-14 13:55:29', 745),
(939, 'schka002', 'Kathryn', 'Schuler', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'kathryn.schuler@staples.com', '2010-05-22', '2010-05-20 12:20:29', 745, '2014-01-24 14:09:18', 745),
(940, 'gauju001', 'Julia', 'Gaudet', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'julia.gaudet@staples.com', '2010-05-29', '2010-05-25 09:16:49', 745, '2014-01-24 14:09:18', 745),
(941, 'ceran001', 'Anthony', 'Cerasuolo', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'anthony.cerasuolo@staples.com', '2010-05-29', '2010-05-27 16:43:27', 745, '2014-01-24 14:09:18', 745),
(942, 'auddi001', 'Diane', 'Audette', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'diane.audette@staples.com', '2010-05-29', '2010-05-27 16:45:56', 745, '2014-01-24 14:09:18', 745),
(943, 'cosda001', 'Dan', 'Costa', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'dan.costa@staples.com', '2010-05-29', '2010-05-27 16:47:41', 745, '2014-01-24 14:09:18', 745),
(944, 'albmi001', 'Mike', 'Alberghene', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'mike.alberghene@staples.com', '2010-05-29', '2010-05-27 16:50:26', 745, '2014-01-24 14:09:18', 745),
(945, 'creanm', 'Michelle', 'Crean', 1, '0000-00-00', '0000-00-00', 1, 1, 796, 'michelle.crean@staples.com', '2010-05-29', '2010-05-28 09:45:32', 873, '2014-01-24 14:09:18', 873),
(946, 'hunda002', 'Danielle', 'Hunter', 2, '0000-00-00', '0000-00-00', 3, 1, 794, 'danielle.hunter@staples.com', '2010-05-29', '2010-05-28 09:56:06', 745, '2014-01-24 14:09:18', 745),
(947, 'cobmi001', 'Michael', 'Cobb', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'michael.cobb@staples.com', '2010-05-29', '2010-05-28 09:58:40', 794, '2014-01-24 14:09:18', 794),
(948, 'mcmry001', 'Ryan', 'McMorrow', 2, '0000-00-00', '0000-00-00', 11, 1, 860, 'ryan.mcmorrow@staples.com', '2010-05-29', '2010-05-28 10:04:57', 745, '2014-01-24 14:09:18', 745),
(949, 'tream002', 'Amanda', 'Trerotola', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'amanda.trerotola@staples.com', '2010-06-05', '2010-06-02 12:31:43', 752, '2014-01-24 14:09:18', 752),
(950, 'ciphe001', 'Henry', 'Cipriano', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'henry.cipriano@staples.com', '2010-06-12', '2010-06-09 10:53:17', 745, '2014-01-24 14:09:18', 745),
(951, 'tenju001', 'Juila', 'Tenney', 2, '0000-00-00', '0000-00-00', 4, 1, 810, 'julia.tenney@staples.com', '2010-06-12', '2010-06-10 11:06:29', 794, '2014-01-24 14:09:18', 794),
(952, 'kouch001', 'Christina', 'Koutrobis', 2, '0000-00-00', '0000-00-00', 7, 1, 843, 'christina.koutrobis@staples.com', '2010-06-19', '2010-06-16 15:51:09', 794, '2014-01-24 14:09:18', 794),
(953, 'hatkr001', 'Kristin', 'Hatcher', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'kristin.hatcher@staples.com', '2010-06-19', '2010-06-16 15:53:37', 873, '2014-01-24 14:09:18', 873),
(954, 'onedo001', 'Doug', 'O''Neill', 1, '0000-00-00', '0000-00-00', 2, 1, 767, 'doug.oneill@staples.com', '2010-07-10', '2010-07-09 16:19:01', 873, '2014-01-24 16:02:19', 873),
(955, 'kaktr001', 'Tripti', 'Kakkar', 2, '0000-00-00', '0000-00-00', 5, 1, 735, 'tripti.kakkar@staples.com', '2010-07-24', '2010-07-20 16:21:06', 794, '2014-01-24 14:09:18', 794),
(957, 'forpa001', 'Paul', 'Fortuin', 2, '0000-00-00', '0000-00-00', 5, 1, 735, 'paul.fortuin@staples.com', '2010-08-07', '2010-08-03 11:13:16', 794, '2014-01-24 14:09:18', 794),
(958, 'gurjo001', 'Joseph', 'Gursky', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'joseph.gursky@staples.com', '2010-08-07', '2010-08-03 11:15:20', 873, '2014-07-10 16:49:39', 873),
(960, 'walan002', 'Andrew', 'Waldrop', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'andrew.waldrop@staples.com', '2010-08-07', '2010-08-03 11:27:15', 745, '2014-01-24 14:09:18', 745),
(961, 'lukre001', 'Renee', 'Lukas', 2, '0000-00-00', '0000-00-00', 5, 1, 773, 'renee.lukas@staples.com', '2010-08-07', '2010-08-03 11:31:17', 745, '2014-01-24 14:09:18', 745),
(963, 'rubsa001', 'Sara', 'Rubinow', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'sara.rubinow@staples.com', '2010-08-16', '2010-08-16 12:57:25', 745, '2014-01-24 14:09:18', 745),
(964, 'calki001', 'Kim', 'Calvi', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'kim.calvi@staples.com', '2010-08-16', '2010-08-16 13:00:32', 745, '2014-01-24 14:09:18', 745),
(965, 'sigsa001', 'Sarah', 'Sigovitch', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'sarah.sigovitch@staples.com', '2010-08-23', '2010-08-16 13:05:46', 745, '2014-01-24 14:09:18', 745),
(966, 'daceykat', 'Katelyn', 'Dacey', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'katelyn.dacey@staples.com', '2010-09-07', '2010-08-16 13:20:16', 873, '2014-07-10 16:49:03', 873),
(967, 'schch002', 'Chris', 'Schmitt', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'chris.schmitt@staples.com', '2010-08-24', '2010-08-24 15:28:15', 745, '2014-01-24 14:09:18', 745),
(968, 'hinal001', 'Alyssa', 'Hinman', 1, '0000-00-00', '0000-00-00', 1, 1, 848, 'alyssa.hinman@staples.com', '2010-08-24', '2010-08-24 15:31:12', 873, '2014-01-24 14:09:18', 873),
(969, 'cohju001', 'Julie', 'Cohen', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'julie.cohen@staples.com', '2010-08-30', '2010-08-27 14:06:02', 745, '2014-01-24 14:09:18', 745),
(970, 'vasge001', 'George', 'Vasiliadis', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'george.vasiliadis@staples.com', '2010-08-30', '2010-08-27 14:10:39', 745, '2014-01-24 14:09:18', 745),
(971, 'wilro002', 'Ronald', 'Willert', 1, '0000-00-00', '0000-00-00', 9, 1, 837, 'ronald.willert@staples.com', '2010-09-01', '2010-09-02 15:18:40', 873, '2014-01-24 14:09:18', 873),
(972, 'gaban001', 'Andrea', 'Gaboriault', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'andrea.gaboriault@staples.com', '2010-09-03', '2010-09-02 15:21:48', 745, '2014-01-24 14:09:18', 745),
(973, 'moopj001', 'PJ', 'Moore', 2, '0000-00-00', '0000-00-00', 1, 1, 777, 'pj.moore@staples.com', '2010-09-02', '2010-09-03 12:16:11', 745, '2014-01-24 14:09:18', 745),
(974, 'camli001', 'Lindsey', 'Campbell', 1, '0000-00-00', '0000-00-00', 5, 1, 773, 'lindsey.campbell@staples.com', '2010-09-27', '2010-09-17 15:27:27', 864, '2014-01-24 14:09:18', 864),
(975, 'kahjo001', 'JoAnn', 'Kahn', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'joann.kahn@staples.com', '2010-09-27', '2010-09-17 15:31:01', 743, '2014-01-24 14:09:18', 743),
(976, 'olsjo001', 'Jo', 'Olson', 1, '0000-00-00', '0000-00-00', 2, 1, 781, 'jo.olson@staples.com', '2010-09-27', '2010-09-24 14:30:44', 873, '2014-01-24 16:02:19', 873),
(977, 'evada001', 'David', 'Evans', 2, '0000-00-00', '0000-00-00', 7, 1, 756, 'david.evans@staples.com', '2010-10-25', '2010-10-26 12:14:56', 745, '2014-01-24 14:09:18', 745),
(978, 'mcicr001', 'Cristina', 'McIntire', 2, '0000-00-00', '0000-00-00', 11, 1, 862, 'cristina.mcintire@staples.com', '2010-11-06', '2010-11-05 10:35:44', 745, '2014-01-24 14:09:18', 745),
(979, 'orete002', 'Terrance', 'O''Reilly', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'terrance.oreilly@staples.com', '2010-11-15', '2010-11-16 14:37:17', 873, '2014-07-10 16:52:55', 873),
(980, 'keljo003', 'John', 'Kelly', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'john.kelly@staples.com', '2010-11-29', '2010-11-30 09:59:30', 873, '2014-01-24 14:09:18', 873),
(981, 'bunge001', 'Gerry', 'Bunker', 1, '0000-00-00', '0000-00-00', 7, 1, 879, 'gerry.bunker@staples.com', '2011-01-08', '2011-01-04 12:20:01', 873, '2014-01-24 14:09:18', 873),
(982, 'petsc001', 'Scott', 'Petrichko', 2, '0000-00-00', '0000-00-00', 1, 1, 777, 'scott.petrichko@staples.com', '2011-01-10', '2011-01-14 12:36:13', 752, '2014-01-24 14:09:18', 752),
(983, 'waler001', 'Eric', 'Walker', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'eric.walker@staples.com', '2011-01-17', '2011-01-28 17:02:40', 794, '2014-01-24 14:09:18', 794),
(984, 'wrire001', 'Rebecca', 'Wright', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'rebeccaelizabeth.wright@staples.com', '2011-01-27', '2011-01-28 17:05:48', 794, '2014-01-24 14:09:18', 794),
(985, 'dessh001', 'Shelagh', 'DeSantis', 2, '0000-00-00', '0000-00-00', 11, 1, 860, 'shelagh.desantis@staples.com', '2011-02-26', '2011-02-24 16:00:49', 745, '2014-01-24 14:09:18', 745),
(987, 'benja001', 'Jane', 'Benson', 2, '0000-00-00', '0000-00-00', 5, 1, 773, 'jane.benson@staples.com', '2011-03-21', '2011-03-21 14:39:15', 752, '2014-01-24 14:09:18', 752),
(988, 'mulca001', 'Carri', 'Mullaney', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'carri.mullaney@staples.com', '2011-04-09', '2011-04-08 12:31:36', 752, '2014-01-24 14:09:18', 752),
(989, 'schri001', 'Richard', 'Schober', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'richard.schober@staples.com', '2011-04-11', '2011-04-11 12:31:38', 752, '2014-01-24 14:09:18', 752),
(990, 'droja001', 'Jacob', 'Drouin', 1, '0000-00-00', '0000-00-00', 2, 1, 731, 'jacob.drouin@staples.com', '2012-02-13', '2011-04-18 15:07:09', 873, '2014-01-24 16:02:19', 873),
(991, 'josal001', 'Alan', 'Joseph', 2, '0000-00-00', '0000-00-00', 5, 1, 773, 'alan.joseph@staples.com', '2012-06-25', '2011-05-18 16:09:31', 745, '2014-01-24 14:09:18', 745),
(992, 'davch001', 'Christopher', 'Davis', 2, '0000-00-00', '0000-00-00', 1, 1, 848, 'christopher.davis@staples.com', '2011-05-31', '2011-06-03 14:46:17', 752, '2014-01-24 14:09:18', 752),
(993, 'desga001', 'Gabriela', 'DeSouza', 2, '0000-00-00', '0000-00-00', 1, 1, 848, 'gabriela.desouza@staples.com', '2011-06-01', '2011-06-03 14:48:40', 794, '2014-01-24 14:09:18', 794),
(994, 'chara001', 'Rachel', 'Chapman', 2, '0000-00-00', '0000-00-00', 1, 1, 791, 'rachel.chapman@staples.com', '2011-06-13', '2011-06-08 10:50:07', 752, '2014-01-24 14:09:18', 752),
(995, 'navkr001', 'Kristen', 'Nave', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'kristen.nave@staples.com', '2011-06-09', '2011-06-08 10:52:30', 752, '2014-01-24 14:09:18', 752),
(996, 'mezca001', 'Carole', 'Mezian', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'carole.mezian@staples.com', '2011-06-20', '2011-06-21 11:15:05', 752, '2014-01-24 14:09:18', 752),
(997, 'uyrha001', 'Haley', 'Uyrus', 2, '0000-00-00', '0000-00-00', 9, 1, NULL, 'haley.uyrus@staples.com', '2013-03-11', '2011-06-21 11:21:16', 745, '2014-04-14 13:54:16', 745),
(998, 'solje001', 'Jessica', 'Solomon', 2, '0000-00-00', '0000-00-00', 2, 1, 781, 'jessica.solomon@staples.com', '2011-07-05', '2011-06-21 11:23:36', 745, '2014-01-24 14:09:18', 745),
(999, 'bagka001', 'Karen', 'Bagley', 2, '0000-00-00', '0000-00-00', 11, 1, 860, 'karen.bagley@staples.com', '2011-06-27', '2011-06-28 10:17:11', 745, '2014-01-24 14:09:18', 745),
(1000, 'reyte001', 'Teale', 'Reynolds', 1, '0000-00-00', '0000-00-00', 5, 1, 837, 'teale.reynolds@staples.com', '2011-08-01', '2011-07-18 11:55:44', 873, '2014-01-24 14:09:18', 873),
(1001, 'walst002', 'Steve', 'Walker', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'steve.walker@staples.com', '2011-07-18', '2011-07-18 11:59:19', 794, '2014-01-24 14:09:18', 794),
(1002, 'warbo001', 'Bob', 'Ward', 2, '0000-00-00', '0000-00-00', 3, 1, 794, 'bob.ward@staples.com', '2011-08-06', '2011-08-02 09:05:03', 752, '2014-01-24 14:09:18', 752),
(1003, 'cruel001', 'El', 'Cruz', 1, '0000-00-00', '0000-00-00', 7, 1, 756, 'el.cruz@staples.com', '2011-08-22', '2011-08-04 16:21:43', 873, '2014-06-06 10:03:17', 873),
(1004, 'bando001', 'Doug', 'Banks', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'doug.banks@staples.com', '2011-08-08', '2011-08-04 16:24:13', 752, '2014-01-24 14:09:18', 752),
(1005, 'merni001', 'Nicole', 'Mercado', 1, '0000-00-00', '0000-00-00', 9, 1, 866, 'nicole.mercado@staples.com', '2011-08-29', '2011-08-24 10:49:13', 794, '2014-02-12 13:49:02', 794),
(1006, 'snyro001', 'Rosemary', 'Snyder', 2, '0000-00-00', '0000-00-00', 7, 1, 848, 'rosemary.snyder@staples.com', '2011-08-29', '2011-08-24 10:54:17', 745, '2014-01-24 14:09:18', 745),
(1007, 'atwmi001', 'Mindy', 'Atwood', 2, '0000-00-00', '0000-00-00', 1, 1, 791, 'mindy.atwood@staples.com', '2011-08-29', '2011-08-24 10:57:00', 752, '2014-01-24 14:09:18', 752),
(1008, 'atham001', 'Amanda', 'Athanasiou', 1, '0000-00-00', '0000-00-00', 1, 1, 876, 'amanda.athanasiou@staples.com', '2011-08-29', '2011-08-26 12:48:31', 873, '2014-01-24 14:09:18', 873);
INSERT INTO `users` (`user_id`, `username`, `first_name`, `last_name`, `status_id`, `status_begin_date`, `status_end_date`, `team_id`, `location_id`, `manager_id`, `email`, `start_date`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1009, 'fanjo001', 'Joanne', 'Fantini', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'joanne.fantini@staples.com', '2011-09-01', '2011-08-31 11:25:14', 752, '2014-01-24 14:09:18', 752),
(1010, 'hanma001', 'Matthew', 'Hannan', 2, '0000-00-00', '0000-00-00', 5, 1, 843, 'matthew.hannan@staples.com', '2011-09-05', '2011-09-02 14:52:20', 873, '2014-01-24 14:09:18', 873),
(1011, 'macch001', 'Charles', 'Mackey', 2, '0000-00-00', '0000-00-00', 4, 1, 844, 'charles.mackey@staples.com', '2011-09-24', '2011-09-21 20:28:42', 752, '2014-01-24 14:09:18', 752),
(1013, 'linta002', 'Tara', 'Lindberg', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'tara.lindberg@staples.com', '2011-10-10', '2011-10-07 13:47:36', 873, '2014-07-10 16:49:57', 873),
(1014, 'lecka001', 'Karen', 'Lecuyer', 1, '0000-00-00', '0000-00-00', 2, 1, 781, 'karen.lecuyer@staples.com', '2014-05-19', '2011-10-07 13:50:54', 873, '2014-05-15 14:58:01', 873),
(1015, 'bowai001', 'Aimee', 'Bowater', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'aimee.bowater@staples.com', '2011-10-17', '2011-10-17 10:18:12', 873, '2014-07-10 16:47:59', 873),
(1016, 'rinka001', 'Katherine', 'Ring', 1, '0000-00-00', '0000-00-00', 1, 1, 727, 'katherine.ring@staples.com', '2011-10-17', '2011-10-17 10:21:50', 873, '2014-01-24 14:09:18', 873),
(1017, 'casje001', 'Jennifer', 'Castillo', 2, '0000-00-00', '0000-00-00', 7, 1, 843, 'jennifer.castillo@staples.com', '2011-10-17', '2011-10-17 11:26:45', 752, '2014-01-24 14:09:18', 752),
(1018, 'lonka001', 'Kate', 'Longueil', 1, '0000-00-00', '0000-00-00', 7, 1, 879, 'kate.longueil@staples.com', '2011-10-29', '2011-10-27 12:59:12', 873, '2014-01-24 14:09:18', 873),
(1019, 'ohu001', 'Hugh', 'O', 2, '0000-00-00', '0000-00-00', 4, 1, 745, 'hugh.o@staples.com', '2011-11-09', '2011-11-09 10:27:53', 873, '2014-01-24 14:09:18', 873),
(1020, 'giuje001', 'Jennifer', 'Giumette', 1, '0000-00-00', '0000-00-00', 7, 1, 756, 'jennifer.giumette@staples.com', '2011-11-14', '2011-11-14 17:37:55', 873, '2014-06-06 10:06:33', 873),
(1021, 'fioda001', 'Dane', 'Fionda', 1, '0000-00-00', '0000-00-00', 5, 1, 866, 'dane.fionda@staples.com', '2011-11-14', '2011-11-14 17:42:24', 794, '2014-02-12 13:48:15', 794),
(1022, 'romsa001', 'Sara', 'Romaine', 2, '0000-00-00', '0000-00-00', 4, 1, 825, 'sara.romaine@staples.com', '2011-11-14', '2011-11-14 17:45:02', 745, '2014-01-24 14:09:18', 745),
(1023, 'patpr002', 'Priscilla', 'Patterson', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'priscilla.patterson@staples.com', '2011-11-14', '2011-11-14 17:48:47', 745, '2014-01-24 14:09:18', 745),
(1024, 'tesan001', 'Andrea', 'Testa', 1, '0000-00-00', '0000-00-00', 7, 1, 837, 'andrea.testa@staples.com', '2011-12-05', '2011-11-29 16:18:40', 873, '2014-01-24 14:09:18', 873),
(1025, 'marbe001', 'Benjamin', 'Marshalkowski', 1, '0000-00-00', '0000-00-00', 5, 1, 837, 'benjamin.marshalkowski@staples.com', '2011-12-05', '2011-11-29 16:21:47', 873, '2014-01-24 14:09:18', 873),
(1026, 'meaol001', 'Olesya', 'Means', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'olesya.means@staples.com', '2011-11-29', '2011-11-29 16:24:22', 752, '2014-01-24 14:09:18', 752),
(1027, 'aliba001', 'Ban', 'Ali', 2, '0000-00-00', '0000-00-00', 1, 1, 843, 'ban.ali@staples.com', '2011-12-12', '2011-11-29 16:26:43', 752, '2014-01-24 14:09:18', 752),
(1028, 'dalla001', 'Laura', 'Dalrymple', 1, '0000-00-00', '0000-00-00', 1, 1, 843, 'laura.dalrymple@staples.com', '2011-12-14', '2011-12-08 17:51:04', 745, '2014-01-24 14:09:18', 745),
(1029, 'rogke001', 'Kelley', 'Rogers', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'kelley.rogers@staples.com', '2012-01-03', '2011-12-08 17:54:22', 873, '2014-01-24 14:09:18', 873),
(1030, 'donbr002', 'Bryan', 'Donovan', 2, '0000-00-00', '0000-00-00', 4, 1, 844, 'bryan.donovan@staples.com', '2011-12-19', '2011-12-19 13:47:42', 752, '2014-01-24 14:09:18', 752),
(1031, 'stajo001', 'Jonathan', 'Stark', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'jonathan.stark@staples.com', '2011-12-30', '2011-12-30 13:45:05', 752, '2014-01-24 14:09:18', 752),
(1032, 'behco001', 'Cory', 'Behrend', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'cory.behrend@staples.com', '2011-12-27', '2011-12-30 13:47:03', 745, '2014-06-03 16:16:27', 745),
(1033, 'folja001', 'Jamie', 'Folsom', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'jamie.folsom@staples.com', '2012-01-04', '2012-01-04 15:59:08', 873, '2014-01-24 14:09:18', 873),
(1034, 'AttCh001', 'Christine', 'Atturio', 1, '0000-00-00', '0000-00-00', 9, 1, 803, 'Christine.Atturio@Staples.com', '2012-01-30', '2012-01-17 10:35:11', 873, '2014-01-24 14:09:18', 873),
(1035, 'MonPh001', 'Philip', 'Mondello', 2, '0000-00-00', '0000-00-00', 2, 1, 855, 'Philip.Mondello@Staples.com', '2012-01-23', '2012-01-25 11:03:15', 745, '2014-01-24 14:09:18', 745),
(1036, 'FisJo001', 'Jon', 'Fish', 2, '0000-00-00', '0000-00-00', 7, 1, 722, 'Jon.Fish@Staples.com', '2012-01-31', '2012-01-27 15:29:17', 873, '2014-01-24 14:09:18', 873),
(1037, 'SpeCa002', 'Casey', 'Spencer', 2, '0000-00-00', '0000-00-00', 7, 1, 848, 'Casey.Spencer@Staples.com', '2012-02-02', '2012-01-27 15:31:44', 873, '2014-01-24 14:09:18', 873),
(1038, 'PurAs002', 'Ashima', 'Purohit', 2, '0000-00-00', '0000-00-00', 7, 1, 814, 'Ashima.Purohit2@Staples.com', '2012-01-04', '2012-02-01 11:02:12', 745, '2014-01-24 14:09:18', 745),
(1039, 'CibFi001', 'Fidel', 'Cibeira', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Fidel.Cibeira@Staples.com', '2013-03-11', '2012-02-08 09:12:20', 745, '2014-01-24 14:09:18', 745),
(1040, 'PyeBr001', 'Brian', 'Pye', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Brian.Pye@Staples.com', '2012-02-06', '2012-02-08 09:13:59', 745, '2014-01-24 14:09:18', 745),
(1041, 'LazCo001', 'Coralee', 'Lazebnikova', 2, '0000-00-00', '0000-00-00', 7, 1, 848, 'Coralee.Lazebnikova@Staples.com', '2012-02-07', '2012-02-08 09:15:30', 873, '2014-01-24 14:09:18', 873),
(1042, 'VelNi001', 'Nicole', 'Vella', 2, '0000-00-00', '0000-00-00', 2, 1, 772, 'Nicole.Vella@Staples.com', '2012-02-13', '2012-02-14 10:49:16', 745, '2014-01-24 14:09:18', 745),
(1045, 'PopKa001', 'Katya', 'Popova', 2, '0000-00-00', '0000-00-00', 1, 1, 777, 'Katya.Popova@Staples.com', '2012-02-27', '2012-02-23 15:54:02', 873, '2014-01-24 14:09:18', 873),
(1046, 'CraDo001', 'Don', 'Crane', 2, '0000-00-00', '0000-00-00', 5, 1, 773, 'Don.Crane@Staples.com', '2012-02-27', '2012-02-23 15:55:20', 873, '2014-01-24 14:09:18', 873),
(1047, 'sarja001', 'Jake', 'Sargent', 2, '0000-00-00', '0000-00-00', 5, 1, 735, 'jake.sargent@staples.com', '2012-02-11', '2012-02-29 10:29:04', 873, '2014-01-24 14:09:18', 873),
(1048, 'logje001', 'Jesse', 'Logan', 2, '0000-00-00', '0000-00-00', 5, 1, 735, 'jesse.logan@staples.com', '2012-03-03', '2012-03-02 10:26:30', 873, '2014-01-24 14:09:18', 873),
(1049, 'CedCh001', 'Chris', 'Cedrone', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Chris.Cedrone@Staples.com', '2012-02-27', '2012-03-05 11:17:18', 745, '2014-01-24 14:09:18', 745),
(1050, 'BellisV', 'Viola', 'Bellis', 2, '0000-00-00', '0000-00-00', 2, 1, 1050, 'Viola.DeBellis@Staples.eu', '2012-03-10', '2012-03-05 12:58:45', 745, '2014-01-24 14:09:18', 745),
(1051, 'EdelbroE', 'Eddy', 'Edelbroek', 2, '0000-00-00', '0000-00-00', 3, 1, NULL, 'Eddy.Edelbroek@Staples.eu', '2012-03-10', '2012-03-05 13:03:37', 745, '2014-01-24 14:09:18', 745),
(1052, 'HAASE', 'Ronald', 'Haasert', 2, '0000-00-00', '0000-00-00', 2, 1, NULL, 'Ronald.Haasert@Staples.eu', '2012-03-10', '2012-03-05 13:04:36', 745, '2014-01-24 14:09:18', 745),
(1053, 'DIJKM', 'Marco', 'Dijk', 2, '0000-00-00', '0000-00-00', 3, 1, NULL, 'Marco.vanDijk@staples.eu', '2012-03-10', '2012-03-05 13:05:39', 745, '2014-01-24 14:09:18', 745),
(1054, 'MohMi001', 'Michelle', 'Mohnkern', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Michelle.Mohnkern@Staples.com', '2012-03-07', '2012-03-06 13:53:48', 745, '2014-01-24 14:09:18', 745),
(1055, 'GemJa001', 'Jacob', 'Gemme', 2, '0000-00-00', '0000-00-00', 4, 1, 844, 'Jacob.Gemme@Staples.com', '2012-03-06', '2012-03-09 09:12:08', 873, '2014-01-24 14:09:18', 873),
(1056, 'ChrAn001', 'Angela', 'Christensen', 2, '0000-00-00', '0000-00-00', 9, 1, NULL, 'Angela.Christensen@Staples.com', '2012-03-19', '2012-03-23 17:10:50', 745, '2014-04-14 13:54:30', 745),
(1057, 'BohDa001', 'Dana', 'Bohne', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Dana.Bohne@Staples.com', '2012-04-02', '2012-04-12 13:19:30', 745, '2014-01-24 14:09:18', 745),
(1058, 'KarJa001', 'Jackie', 'Karnoff', 1, '0000-00-00', '0000-00-00', 7, 1, 848, 'Jackie.Karnoff@Staples.com', '2012-03-30', '2012-04-12 13:21:41', 873, '2014-01-24 14:09:18', 873),
(1059, 'chrisconti', 'Chris', 'Conti', 2, '0000-00-00', '0000-00-00', 1, 1, 727, 'Chris.Conti@Staples.com', '2012-05-12', '2012-05-10 12:43:38', 873, '2014-06-16 10:44:42', 873),
(1060, 'BreEd001', 'Edie', 'Bresler', 2, '0000-00-00', '0000-00-00', 1, 1, 727, 'Edie.Bresler@Staples.com', '2012-05-12', '2012-05-10 12:44:36', 873, '2014-01-24 14:09:18', 873),
(1061, 'MenEl003', 'Elizabeth', 'Mendelsohn', 1, '0000-00-00', '0000-00-00', 7, 1, 876, 'Elizabeth.Mendelsohn@Staples.com', '2012-05-21', '2012-05-18 09:38:13', 873, '2014-01-24 14:09:18', 873),
(1062, 'OsgDa001', 'David', 'Osgood', 1, '0000-00-00', '0000-00-00', 7, 1, 879, 'David.Osgood@Staples.com', '2012-05-14', '2012-05-18 09:40:11', 745, '2014-04-23 09:28:30', 745),
(1064, 'PPharo', 'Pandora', 'Pharo', 1, '0000-00-00', '0000-00-00', 1, 2, 1086, 'Pandora.Pharo@staples.ca', '2012-11-01', '2012-05-30 11:40:47', 873, '2014-01-24 14:09:18', 873),
(1067, 'krojo001', 'Jodi', 'Kroop', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'jodi.kroop@staples.com', '2012-05-30', '2012-05-30 12:58:28', 745, '2014-01-24 14:09:18', 745),
(1068, 'ritmi001', 'Michael', 'Ritter', 2, '0000-00-00', '0000-00-00', 9, 1, 727, 'Michael.Ritter@Staples.com', '2012-06-06', '2012-06-06 14:44:45', 745, '2014-04-14 13:54:47', 745),
(1069, 'HaaSa001', 'Sarah', 'Haag', 2, '0000-00-00', '0000-00-00', 1, 1, 727, 'Sarah.Haag@Staples.com', '2012-06-04', '2012-06-06 14:46:41', 745, '2014-04-14 13:57:22', 745),
(1070, 'CinAb001', 'Abigail', 'Cinamon', 2, '0000-00-00', '0000-00-00', 2, 1, 781, 'Abigail.Cinamon@Staples.com', '2012-06-11', '2012-06-06 14:49:19', 745, '2014-01-24 14:09:18', 745),
(1071, 'licsi001', 'Simon', 'Lichter', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'Simon.Lichter@Staples.com', '2012-06-11', '2012-06-11 12:03:18', 745, '2014-01-24 14:09:18', 745),
(1072, 'KinFr001', 'Frank', 'Kingsley', 2, '0000-00-00', '0000-00-00', 2, 1, 828, 'Frank.Kingsley@Staples.com', '2012-06-18', '2012-06-11 12:05:12', 745, '2014-01-24 14:09:18', 745),
(1073, 'BolKa001', 'Kathy', 'Boluch', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Kathy.Boluch@Staples.com', '2012-06-18', '2012-06-21 12:51:29', 745, '2014-01-24 14:09:18', 745),
(1074, 'BurFa001', 'Faith', 'Burgos', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Faith.Burgos@Staples.com', '2012-06-20', '2012-06-21 12:52:34', 745, '2014-04-14 14:01:01', 745),
(1075, 'DukRo001', 'Robin', 'Duke', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Robin.Duke@Staples.com', '2012-06-20', '2012-06-21 12:53:40', 745, '2014-01-24 14:09:18', 745),
(1076, 'ThoJo001', 'John', 'Thompson', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'John.Thompson@Staples.com', '2012-06-19', '2012-06-21 12:55:01', 745, '2014-01-24 14:09:18', 745),
(1077, 'CapAm001', 'Amy', 'Capomaccio', 2, '0000-00-00', '0000-00-00', 5, 1, 777, 'Amy.Capomaccio@Staples.com', '2012-06-25', '2012-06-27 12:46:07', 745, '2014-04-14 13:55:47', 745),
(1078, 'MalinKim', 'Kimberly', 'Malin', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'Kimberly.Malin@Staples.com', '2012-07-06', '2012-07-09 10:41:49', 745, '2014-02-06 17:27:01', 745),
(1079, 'dlee', 'Dickey', 'Lee', 1, '0000-00-00', '0000-00-00', 1, 2, NULL, 'Dickey.Lee@staples.ca', '2012-07-14', '2012-07-10 14:01:28', 873, '2014-01-24 14:09:18', 873),
(1081, 'SSalmon', 'Sandy', 'Salmon', 1, '0000-00-00', '0000-00-00', 6, 2, 843, 'Sandy.Salmon@Staples.ca', '2012-07-14', '2012-07-10 14:03:25', 873, '2014-01-24 16:07:29', 873),
(1082, 'DSilva', 'Donna', 'Silva', 1, '0000-00-00', '0000-00-00', 2, 2, 731, 'Donna.Silva@staples.ca', '2012-07-14', '2012-07-10 14:04:56', 745, '2014-01-24 16:02:19', 745),
(1083, 'NCruz', 'Noel', 'Cruz', 1, '0000-00-00', '0000-00-00', 1, 2, 1081, 'Noel.Cruz@staples.ca', '2012-07-14', '2012-07-10 14:06:44', 873, '2014-01-24 14:09:18', 873),
(1084, 'KPetit', 'Kelly', 'Petit', 1, '0000-00-00', '0000-00-00', 1, 2, 1081, 'Kelly.Petit@staples.ca', '2012-07-14', '2012-07-10 14:07:55', 873, '2014-01-24 16:07:29', 873),
(1085, 'DMarrin', 'Donna', 'Marrin', 1, '0000-00-00', '0000-00-00', 5, 2, 1081, 'Donna.Marrin@Staples.ca', '2012-07-14', '2012-07-10 14:09:51', 873, '2014-01-24 14:09:18', 873),
(1086, 'PCuison', 'Pamela', 'Cuison', 1, '0000-00-00', '0000-00-00', 7, 2, 796, 'Pamela.Cuison@Staples.ca', '2012-07-14', '2012-07-10 14:13:09', 776, '2014-01-24 14:09:18', 776),
(1087, 'YanAn001', 'Andrew', 'Yang', 2, '0000-00-00', '0000-00-00', 7, 2, 1086, 'Andrew.Yang@staples.ca', '2012-07-14', '2012-07-10 14:34:27', 873, '2014-01-24 14:09:18', 873),
(1089, 'RLor', 'Richard', 'Lor', 1, '0000-00-00', '0000-00-00', 7, 2, 1086, 'Richard.Lor@staples.ca', '2012-07-14', '2012-07-10 15:41:14', 873, '2014-01-24 14:09:18', 873),
(1090, 'TGuido', 'Tiziana', 'Guido', 2, '0000-00-00', '0000-00-00', 1, 2, 1081, 'Tiziana.Guido@staples.ca', '2012-07-14', '2012-07-13 12:07:02', 873, '2014-01-24 14:09:18', 873),
(1091, 'BParadis', 'Brigitte', 'Paradis', 1, '0000-00-00', '0000-00-00', 5, 2, 1084, 'brigitte.paradis@staples.ca', '2012-07-14', '2012-07-13 12:09:59', 873, '2014-01-24 14:09:18', 873),
(1092, 'CLeggett', 'Chris', 'Leggett', 1, '0000-00-00', '0000-00-00', 1, 2, 1083, 'Chris.Leggett@staples.ca', '2012-07-14', '2012-07-13 12:11:17', 873, '2014-01-24 14:09:18', 873),
(1093, 'BMcNair', 'Barbara', 'McNair', 1, '0000-00-00', '0000-00-00', 1, 2, 1083, 'Barbara.McNair@staples.ca', '2012-07-14', '2012-07-13 12:12:37', 873, '2014-05-27 14:02:40', 873),
(1094, 'TNasso', 'Antonio', 'Nasso', 1, '0000-00-00', '0000-00-00', 1, 2, 1083, 'antonio.Nasso@staples.ca', '2012-07-14', '2012-07-13 12:13:44', 873, '2014-05-27 14:02:24', 873),
(1095, 'GHunter', 'Greg', 'Hunter', 1, '0000-00-00', '0000-00-00', 1, 2, 1083, 'Greg.Hunter@staples.ca', '2012-07-14', '2012-07-13 12:16:24', 873, '2014-01-24 14:09:18', 873),
(1096, 'AKuma', 'Alex', 'Kuma', 2, '0000-00-00', '0000-00-00', 1, 2, 1084, 'Alex.Kuma@staples.ca', '2012-07-14', '2012-07-13 12:18:02', 745, '2014-04-14 15:21:48', 745),
(1097, 'TMarvin', 'Tom', 'Marvin', 1, '0000-00-00', '0000-00-00', 1, 1, 1084, 'Tom.Marvin@staples.c', '2012-07-14', '2012-07-13 12:19:25', 873, '2014-01-24 14:09:18', 873),
(1098, 'KBrangers', 'Kerri', 'Brangers', 1, '0000-00-00', '0000-00-00', 1, 2, 1084, 'Kerri.Brangers2@staples.ca', '2012-07-14', '2012-07-13 12:20:40', 873, '2014-01-24 14:09:18', 873),
(1099, 'SchDa003', 'David', 'Schump', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'David.Schump@Staples.com', '2012-07-17', '2012-07-24 16:48:14', 745, '2014-04-14 13:57:42', 745),
(1100, 'BlaJo004', 'Joanne', 'Blank', 1, '0000-00-00', '0000-00-00', 1, 1, 777, 'Joanne.Blank@Staples.com', '2012-07-24', '2012-07-24 16:49:47', 873, '2014-01-24 14:09:18', 873),
(1101, 'tayja001', 'Jane', 'Taylor', 1, '0000-00-00', '0000-00-00', 2, 1, 1243, 'jane.taylor@staples.com', '2014-07-21', '2012-08-01 14:13:36', 873, '2014-07-30 11:39:33', 873),
(1102, '', 'CSS', 'CSS', 2, '0000-00-00', '0000-00-00', 4, 1, 825, '', '2012-08-04', '2012-08-03 10:21:15', 745, '2014-04-14 09:52:03', 745),
(1103, 'walst001', 'Steven', 'Walker', 1, '0000-00-00', '0000-00-00', 7, 1, 1246, 'steven.walker@staples.com', '2012-08-06', '2012-08-06 14:53:12', 873, '2014-01-24 14:09:18', 873),
(1104, 'hanch003', 'Christian', 'Hansen', 1, '0000-00-00', '0000-00-00', 7, 1, 756, 'Christian.Hansen@Staples.com', '2014-07-28', '2012-08-10 15:28:40', 873, '2014-07-30 09:57:39', 873),
(1105, 'DeNKr001', 'Kristen', 'DeNapoli', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Kristen.DeNapoli@Staples.com', '2012-08-14', '2012-08-15 15:24:45', 745, '2014-01-24 14:09:18', 745),
(1107, 'TorBr001', 'Bryan', 'Torgerson', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'Bryan.Torgerson@Staples.com', '2012-08-24', '2012-08-27 12:55:11', 745, '2014-04-14 13:57:58', 745),
(1108, 'IKupidy', 'Ian', 'Kupidy', 1, '0000-00-00', '0000-00-00', 2, 2, 731, 'Ian.Kupidy@staples.ca', '2012-08-22', '2012-08-27 12:56:24', 873, '2014-01-24 16:02:19', 873),
(1109, 'ShaSa002', 'Sam', 'Shaughnessy', 2, '0000-00-00', '0000-00-00', 5, 1, 735, 'Sam.Shaughnessy@Staples.com', '2012-08-30', '2012-09-05 12:14:52', 873, '2014-01-24 14:09:18', 873),
(1110, 'HarMe001', 'Melissa', 'Harrison', 2, '0000-00-00', '0000-00-00', 1, 1, 727, 'Melissa.Harrison@Staples.com', '2012-09-10', '2012-09-12 10:15:15', 745, '2014-04-14 09:51:39', 745),
(1111, 'MRageas', 'Madeline', 'Rageas', 1, '0000-00-00', '0000-00-00', 2, 2, 1084, 'Madeline.Rageas@staples.ca', '2012-09-15', '2012-09-12 10:18:23', 873, '2014-01-24 16:02:19', 873),
(1112, 'ClaBr002', 'Bret', 'Clancy', 1, '0000-00-00', '0000-00-00', 2, 1, 781, 'Bret.Clancy@Staples.com', '2012-09-17', '2012-09-19 12:42:34', 873, '2014-01-24 16:02:19', 873),
(1113, 'DowTi001', 'Timothy', 'Dowd', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'Timothy.Dowd@Staples.com', '2012-09-24', '2012-09-20 09:24:40', 873, '2014-07-10 16:53:49', 873),
(1114, 'HilAd001', 'Adam', 'Hill', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'Adam.Hill@Staples.com', '2012-09-24', '2012-09-20 09:26:25', 873, '2014-07-10 16:55:18', 873),
(1115, 'UksMi001', 'Michael', 'Ukstins', 2, '0000-00-00', '0000-00-00', 1, 1, 735, 'Michael.Ukstins@Staples.com', '2012-09-12', '2012-09-20 09:29:41', 873, '2014-01-24 14:09:18', 873),
(1116, 'smiro004', 'Rod', 'Smith', 2, '0000-00-00', '0000-00-00', 1, 1, 735, 'rod.smith@staples.com', '2012-09-07', '2012-09-20 09:39:21', 745, '2014-01-24 14:09:18', 745),
(1117, 'macke003', 'Kevin', 'MacKenzie', 2, '0000-00-00', '0000-00-00', 5, 1, 829, 'kevin.mackenzie@staples.com', '2012-09-13', '2012-09-20 09:40:58', 873, '2014-01-24 14:09:18', 873),
(1118, 'BarEl001', 'Elena', 'Barbera', 1, '0000-00-00', '0000-00-00', 5, 1, 728, 'Elena.Barbera@Staples.com', '2013-10-15', '2012-09-20 09:43:00', 873, '2014-03-25 10:42:04', 873),
(1119, 'SprGr001', 'Graham', 'Sprague', 1, '0000-00-00', '0000-00-00', 3, 1, 745, 'Graham.Sprague@Staples.com', '2012-10-01', '2012-09-28 15:08:33', 1119, '2014-08-11 13:09:39', 1308),
(1120, 'MarMi006', 'Michelle', 'Martin', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'Michelle.Martin@Staples.com', '2012-10-01', '2012-09-28 15:14:47', 745, '2014-04-14 13:58:12', 745),
(1121, 'DalWi001', 'Winston', 'Dalip', 2, '0000-00-00', '0000-00-00', 7, 2, 1086, 'Winston.Dalip@staples.ca', '2012-10-15', '2012-10-15 16:58:02', 873, '2014-01-24 14:09:18', 873),
(1122, 'WilKe003', 'Kelly', 'Wilson-Wight', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'Kelly.Wilson-Wight@Staples.com', '2012-10-15', '2012-10-16 14:06:26', 745, '2014-01-24 14:09:18', 745),
(1123, 'BoyDa001', 'Dawn', 'Boyer', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Dawn.Boyer@Staples.com', '2012-10-12', '2012-10-16 14:07:54', 745, '2014-01-24 14:09:18', 745),
(1124, 'QuiDa001', 'Dan', 'Quigley', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Dan.Quigley@Staples.com', '2012-10-12', '2012-10-16 14:09:16', 745, '2014-01-24 14:09:18', 745),
(1125, 'DoaEr001', 'Eric', 'Doan', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Eric.Doan@Staples.com', '2012-10-12', '2012-10-16 14:12:28', 745, '2014-01-24 14:09:18', 745),
(1126, 'SwePa003', 'Patrice', 'Sweeney', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Patrice.Sweeney@Staples.com', '2012-10-12', '2012-10-23 14:25:11', 745, '2014-01-24 14:09:18', 745),
(1127, 'PelMa001', 'Margaret', 'Pelton', 2, '0000-00-00', '0000-00-00', 2, 1, 801, 'Margaret.Pelton@Staples.com', '2012-10-16', '2012-10-23 14:26:33', 745, '2014-01-24 14:09:18', 745),
(1128, 'JolDa001', 'Dale', 'Jolliffee', 1, '0000-00-00', '0000-00-00', 1, 1, 765, 'Dale.Jolliffee@Staples.com', '2012-10-22', '2012-10-23 14:28:34', 873, '2014-01-24 14:09:18', 873),
(1129, 'mursa001', 'Sara', 'Murphy', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'Sara.Murphy@Staples.com', '2012-10-29', '2012-10-23 14:30:40', 873, '2014-01-24 14:09:18', 873),
(1130, 'HalNa002', 'Nancy', 'Hall', 2, '0000-00-00', '0000-00-00', 2, 1, 801, 'Nancy.Hall@Staples.com', '2012-10-18', '2012-10-31 11:43:51', 745, '2014-01-24 14:09:18', 745),
(1131, 'GagCh001', 'Christopher', 'Gagnon', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Christopher.Gagnon@Staples.com', '2012-10-24', '2012-10-31 13:30:10', 745, '2014-01-24 14:09:18', 745),
(1132, 'TadMa001', 'Mario', 'Taddeo', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Mario.Taddeo@Staples.com', '2012-10-17', '2012-10-31 13:31:44', 745, '2014-01-24 14:09:18', 745),
(1133, 'NalAr001', 'Armeen', 'Nalladaru', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'Armeen.Nalladaru@Staples.com', '2012-11-05', '2012-11-07 10:24:56', 745, '2014-04-14 13:58:25', 745),
(1134, 'MarAn002', 'Ann', 'Marks', 2, '0000-00-00', '0000-00-00', 1, 1, 777, 'Ann.Marks@Staples.com', '2012-11-12', '2012-11-12 10:10:12', 745, '2014-04-14 13:58:36', 745),
(1135, 'FunSt001', 'Steven', 'Fund', 2, '0000-00-00', '0000-00-00', 6, 1, 1135, 'Steven.Fund@Staples.com', '2012-11-17', '2012-11-13 14:05:50', 873, '2014-06-16 11:16:35', 873),
(1136, 'BarEl002', 'Elizabeth', 'Bartosiewicz', 2, '0000-00-00', '0000-00-00', 2, 1, 868, 'Elizabeth.Bartosiewicz@Staples.com', '2013-01-07', '2012-12-18 11:21:29', 873, '2014-01-24 14:09:18', 873),
(1137, 'nagma002', 'Matt', 'Nagy', 2, '0000-00-00', '0000-00-00', 1, 1, 837, 'Matt.Nagy@Staples.com', '2013-01-08', '2013-01-07 17:14:50', 873, '2014-01-24 14:09:18', 873),
(1138, 'ParCl001', 'Claudia', 'Paredes', 2, '0000-00-00', '0000-00-00', 5, 1, NULL, 'Claudia.Paredes@Staples.com', '2013-01-15', '2013-01-18 14:55:32', 745, '2014-04-14 13:56:05', 745),
(1139, 'LyoCa001', 'Caleb', 'Lyons', 2, '0000-00-00', '0000-00-00', 7, 1, 756, 'Caleb.Lyons@Staples.com', '2013-01-22', '2013-01-23 16:44:31', 873, '2014-01-24 14:09:18', 873),
(1140, 'cursa001', 'Samantha', 'Curtin', 1, '0000-00-00', '0000-00-00', 4, 1, 966, 'Samantha.Curtin@Staples.com', '2013-01-28', '2013-01-29 14:48:47', 873, '2014-07-10 16:52:31', 873),
(1141, 'pfuji001', 'Jillian', 'Pfund', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'Jillian.Pfund@Staples.com', '2013-01-29', '2013-01-29 14:54:18', 745, '2014-04-14 13:58:51', 745),
(1142, 'craky001', 'Kyle', 'Cranston', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'Kyle.Cranston@Staples.com', '2013-01-29', '2013-01-29 14:56:51', 745, '2014-04-14 13:59:03', 745),
(1143, 'mccmi007', 'Michael', 'McCann', 2, '0000-00-00', '0000-00-00', 5, 1, NULL, 'Michael.McCann@Staples.com', '2013-01-28', '2013-01-29 14:59:23', 745, '2014-04-14 13:56:20', 745),
(1144, 'helga001', 'Gabrielle', 'Helfgott', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'Gabrielle.Helfgott@Staples.com', '2013-02-04', '2013-02-05 16:52:30', 745, '2014-01-24 14:09:18', 745),
(1145, 'poiaa001', 'Aaron', 'Poisson', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'Aaron.Poisson@Staples.com', '2013-02-05', '2013-02-05 16:55:15', 745, '2014-04-14 13:59:21', 745),
(1146, 'WesMi001', 'Michelle', 'Wesley', 2, '0000-00-00', '0000-00-00', 2, 1, NULL, 'Michelle.Wesley@Staples.com', '2013-02-05', '2013-02-11 14:07:44', 745, '2014-01-24 14:09:18', 745),
(1147, 'morra003', 'Ralph', 'Moreau', 2, '0000-00-00', '0000-00-00', 1, 1, 796, 'Ralph.Moreau@Staples.com', '2013-02-11', '2013-02-11 16:48:38', 873, '2014-01-24 14:09:18', 873),
(1148, 'houje003', 'Jessalyn', 'House', 1, '0000-00-00', '0000-00-00', 4, 1, 966, 'JESSALYN.HOUSE@Staples.com', '2013-02-11', '2013-02-11 16:53:01', 873, '2014-07-10 16:52:10', 873),
(1149, 'ngsi001', 'Simon', 'Ng', 2, '0000-00-00', '0000-00-00', 1, 1, NULL, 'Simon.Ng@Staples.com', '2013-02-06', '2013-02-11 16:56:10', 745, '2014-04-14 13:59:33', 745),
(1150, 'SaiYa001', 'Yam', 'Saiki', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Yam.Saiki@Staples.com', '2013-02-12', '2013-02-13 16:54:06', 745, '2014-04-14 14:01:32', 745),
(1151, 'TisJe001', 'Jennifer', 'Tisdale', 2, '0000-00-00', '0000-00-00', 2, 1, 748, 'Jennifer.Tisdale@Staples.com', '2013-02-08', '2013-02-13 16:57:58', 745, '2014-01-24 14:09:18', 745),
(1152, 'HowKa004', 'Kathy', 'Howard', 2, '0000-00-00', '0000-00-00', 1, 1, 796, 'Kathy.Howard@Staples.com', '2013-02-11', '2013-02-13 17:00:31', 873, '2014-01-24 14:09:18', 873),
(1153, 'JonLa001', 'Lauren', 'Jones', 1, '0000-00-00', '0000-00-00', 1, 1, 727, 'Lauren.Jones@Staples.com', '2013-02-15', '2013-02-19 16:24:20', 873, '2014-06-04 16:49:52', 873),
(1154, 'CamLi001', 'Lindsey', 'Campbell', 2, '0000-00-00', '0000-00-00', 5, 1, 848, 'Lindsey.Campbell@Staples.com', '2013-02-18', '2013-02-19 16:29:49', 873, '2014-01-24 14:09:18', 873),
(1155, 'WonDa001', 'Daniel', 'Wong', 2, '0000-00-00', '0000-00-00', 1, 1, 777, 'Daniel.Wong@Staples.com', '2013-02-13', '2013-02-19 16:33:53', 745, '2014-04-14 13:59:52', 745),
(1156, 'WilLi001', 'Lindsey', 'Wilhelm', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Lindsey.Wilhelm@Staples.com', '2013-02-25', '2013-02-21 11:57:23', 745, '2014-04-14 14:01:52', 745),
(1157, 'BarNa002', 'Nadia', 'Barber', 2, '0000-00-00', '0000-00-00', 9, 1, NULL, 'Nadia.Barbe@Staples.com', '2013-02-19', '2013-02-21 13:24:05', 745, '2014-04-14 13:55:10', 745),
(1158, 'BogTa001', 'Tania', 'Boghossian', 2, '0000-00-00', '0000-00-00', 5, 1, NULL, 'Tania.Boghossian@Staples.com', '2013-02-20', '2013-02-21 13:48:57', 745, '2014-04-14 13:56:33', 745),
(1160, 'petsa001', 'Sarah', 'Petrie', 1, '0000-00-00', '0000-00-00', 5, 1, 848, 'Sarah.Petrie@Staples.com', '2013-03-11', '2013-03-04 15:02:39', 864, '2014-01-24 14:09:18', 864),
(1161, 'JohBe001', 'Ben', 'Johnson', 2, '0000-00-00', '0000-00-00', 5, 1, 765, 'Ben.Johnson@Staples.com', '2013-03-01', '2013-03-04 15:07:16', 745, '2014-04-14 13:56:44', 745),
(1162, 'SouDe001', 'Derek', 'Sousa', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'Derek.Sousa@Staples.com', '2013-03-11', '2013-03-04 16:59:16', 873, '2014-07-10 16:54:52', 873),
(1163, 'ClaVa001', 'Vanna', 'Clark', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Vanna.Clark@Staples.Com', '2013-02-28', '2013-03-04 17:01:27', 745, '2014-01-24 14:09:18', 745),
(1164, 'FiuJi001', 'Jillian', 'Fiumara', 2, '0000-00-00', '0000-00-00', 2, 1, 731, 'Jillian.Fiumara@Staples.com', '2013-03-25', '2013-03-11 10:57:17', 745, '2014-06-03 16:17:34', 745),
(1165, 'BaiFe001', 'Felicia', 'Bailote', 2, '0000-00-00', '0000-00-00', 2, 1, 1243, 'Felicia.Bailote@Staples.com', '2013-03-18', '2013-03-14 09:46:03', 873, '2014-01-24 14:09:18', 873),
(1166, 'GalKi001', 'Kimberly', 'Gallant', 1, '0000-00-00', '0000-00-00', 2, 1, 781, 'Kimberly.Gallant@Staples.com', '2013-03-25', '2013-03-14 09:48:37', 873, '2014-01-24 16:02:19', 873),
(1167, 'robertallen', 'Robert', 'Allen', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'robert.allen@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1168, 'reynoldsansley', 'Reynolds', 'Ansley', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'reynolds.ansley@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1169, 'tracybadiola', 'Tracy', 'Badiola', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'Tracy.Badiola@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1170, 'randybadiola', 'Randy', 'Badiola', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'randolph.badiola@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1171, 'carriebasgall', 'Carrie', 'Basgall', 2, '0000-00-00', '0000-00-00', 1, 3, 1201, 'carrie.basgall@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1172, 'brianrice', 'Brian', 'Rice', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'brian.rice@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1173, 'jeffcollazo', 'Jeff', 'Collazo', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'jeffrey.collazo@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1174, 'dougdanley', 'Doug', 'Danley', 2, '0000-00-00', '0000-00-00', 1, 3, NULL, 'Doug.Danley@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1175, 'heididepue', 'Heidi', 'DePue', 1, '0000-00-00', '0000-00-00', 2, 3, 1234, 'heidi.depue@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 16:05:14', 873),
(1176, 'alexfedec', 'Alex', 'Fedec', 2, '0000-00-00', '0000-00-00', 5, 3, 1198, 'alex.fedec@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1177, 'heathergrayson', 'Heather', 'Grayson', 1, '0000-00-00', '0000-00-00', 4, 3, 1201, 'Heather.Grayson@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1178, 'sherylharris', 'Sheryl', 'Harris', 2, '0000-00-00', '0000-00-00', 1, 3, NULL, 'sheryl.harris@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1179, 'robertjacob', 'Robert', 'Jacob', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'robert.jacob@quill.com', '2013-10-19', '2013-03-14 21:28:01', 873, '2014-06-04 13:09:59', 873),
(1180, 'amandajohnson', 'Amanda', 'Johnson', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'Amanda.Johnson@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1181, 'monikaland', 'Monika', 'Land', 2, '0000-00-00', '0000-00-00', 4, 3, 1198, 'monika.land@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1182, 'lauralange', 'Laura', 'Lange', 2, '0000-00-00', '0000-00-00', 1, 3, NULL, 'laura.lange@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1183, 'richlinden', 'Rich', 'Linden', 1, '0000-00-00', '0000-00-00', 5, 3, 1198, 'rich.linden@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1184, 'cameronmasiclat', 'Cam', 'Masiclat', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'cameron.masiclat@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1185, 'adammcamis', 'Adam', 'McAmis', 2, '0000-00-00', '0000-00-00', 1, 3, 1186, 'Adam.McAmis@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1186, 'kevinmikolajewski', 'Kevin', 'Mikolajewski', 2, '0000-00-00', '0000-00-00', 1, 3, 843, 'kevin.mikolajewski@quill.com', '2013-03-14', '2013-03-14 21:28:01', 745, '2014-01-24 14:09:18', 745),
(1187, 'efrainmontoya', 'Efrain', 'Montoya', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'Efrain.Montoya@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1188, 'andremoore', 'Andre', 'Moore', 2, '0000-00-00', '0000-00-00', 5, 3, 1198, 'andre.moore@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1189, 'stevemurawski', 'Steve', 'Murawski', 2, '0000-00-00', '0000-00-00', 1, 3, NULL, 'steve.murawski@quill.com', '2013-03-14', '2013-03-14 21:28:01', 745, '2014-06-03 16:16:45', 745),
(1190, 'tomnovotny', 'Tom', 'Novotny', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'tom.novotny@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1191, 'wallypyles', 'Wally', 'Pyles', 1, '0000-00-00', '0000-00-00', 3, 3, 1119, 'wally.pyles@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-04-11 16:39:01', 873),
(1192, 'christinerieker', 'Christine', 'Rieker', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'christine.rieker@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1193, 'lindaromanow', 'Linda', 'Romanow', 1, '0000-00-00', '0000-00-00', 1, 3, 1198, 'linda.romanow@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1194, 'stephenschaul', 'Stephen', 'Schaul', 2, '0000-00-00', '0000-00-00', 1, 3, NULL, 'stephen.schaul@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1195, 'colleenschmitt', 'Colleen', 'Schmitt', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'colleen.schmitt@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1196, 'rycashih', 'Ryca', 'Shih', 2, '0000-00-00', '0000-00-00', 1, 3, 1186, 'Ryca.Shih@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1197, 'thomasshipley', 'Tom', 'Shipley', 1, '0000-00-00', '0000-00-00', 5, 3, 735, 'thomas.shipley@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1198, 'wendysiegel', 'Wendy', 'Siegel', 1, '0000-00-00', '0000-00-00', 4, 3, 825, 'wendy.siegel@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1199, 'gregskarbalus', 'Greg', 'Skarbalus', 1, '0000-00-00', '0000-00-00', 5, 3, 735, 'greg.skarbalus@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1200, 'amiesmith', 'Amie', 'Smith', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'amie.smith@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1201, 'katiestrezo', 'Katie', 'Strezo', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'Katie.Strezo@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1202, 'jenniferturner', 'Jennifer', 'Turner', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'Jennifer.Turner@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1203, 'pamelavictor', 'Pam', 'Victor', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'pamela.victor@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1204, 'brianwenberg', 'Brian', 'Wenberg', 2, '0000-00-00', '0000-00-00', 4, 3, 1168, 'brian.wenberg@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1205, 'lindawhite', 'Linda', 'White', 2, '0000-00-00', '0000-00-00', 4, 3, 1198, 'linda.white@quill.com', '2013-03-14', '2013-03-14 21:28:01', 745, '2014-01-24 14:09:18', 745),
(1206, 'benwilson', 'Ben', 'Wilson', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'ben.wilson@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1207, 'kimberliwinkler', 'Kimberli', 'Winkler', 1, '0000-00-00', '0000-00-00', 5, 3, 735, 'Kimberli.Winkler@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1208, 'christywolf', 'Christy', 'Wolf', 2, '0000-00-00', '0000-00-00', 4, 3, 1201, 'christy.wolf@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1209, 'debrathompson', 'Deb', 'Thompson', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'debra.thompson@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-01-24 14:09:18', 873),
(1210, 'edwardschwarz', 'Ed', 'Schwarz', 1, '0000-00-00', '0000-00-00', 1, 3, NULL, 'edward.schwarz@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1211, 'phillipdestefano', 'Phil', 'Stefano', 2, '0000-00-00', '0000-00-00', 1, 3, NULL, 'phillip.destefano@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-01-24 14:09:18', 1191),
(1212, 'andrewedmonds', 'Andy', 'Edmonds', 1, '0000-00-00', '0000-00-00', 5, 3, NULL, 'andrew.edmonds@quill.com', '2013-03-14', '2013-03-14 21:28:01', 1191, '2014-03-14 16:42:18', 1191),
(1213, 'zibiszczalba', 'Zibi', 'Szczalba', 1, '0000-00-00', '0000-00-00', 3, 3, 1191, 'zibi.szczalba@quill.com', '2013-03-14', '2013-03-14 21:28:01', 873, '2014-04-11 16:39:16', 873),
(1214, 'lydiamaloney', 'Lydia', 'Maloney', 1, '0000-00-00', '0000-00-00', 2, 3, 1234, 'lydia.maloney@quill.com', '2013-03-18', '2013-03-18 14:19:17', 873, '2014-01-24 16:05:14', 873),
(1215, 'leahdantonio', 'Leah', 'D''Antonio', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'Leah.D''Antonio@quill.com', '2013-03-18', '2013-03-18 14:20:42', 873, '2014-01-24 14:09:18', 873),
(1216, 'tanyakornikova', 'Tanya', 'Kornikova', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'Tanya.Kornikova@quill.com', '2013-03-18', '2013-03-18 14:21:49', 873, '2014-01-24 14:09:18', 873),
(1217, 'arpitamanjrekar', 'Arpita', 'Manjrekar', 1, '0000-00-00', '0000-00-00', 1, 3, 1198, 'Arpita.Manjrekar@quill.com', '2013-03-18', '2013-03-18 14:22:59', 1191, '2014-01-24 14:09:18', 1191),
(1218, 'AdaMi001', 'Michele', 'Adamo', 2, '0000-00-00', '0000-00-00', 2, 1, 731, 'Michele.Adamo@Staples.com', '2013-03-18', '2013-03-20 17:19:33', 873, '2014-01-24 14:09:18', 873),
(1219, 'PatSh002', 'Shazia', 'Pathan', 2, '0000-00-00', '0000-00-00', 12, 2, 1086, 'Shazia.Pathan@staples.ca', '2013-02-19', '2013-03-22 14:46:00', 873, '2014-01-24 14:09:18', 873),
(1220, 'angelaquinlan', 'Angela', 'Quinlan', 2, '0000-00-00', '0000-00-00', 2, 3, 1234, 'angela.quinlan@quill.com', '2013-03-18', '2013-03-25 11:29:53', 745, '2014-04-14 14:57:27', 745),
(1221, 'BraJo002', 'Joseph', 'Brazell', 2, '0000-00-00', '0000-00-00', 7, 1, NULL, 'Joseph.Brazell@Staples.com', '2013-03-25', '2013-03-28 14:55:24', 745, '2014-04-14 14:02:15', 745),
(1222, 'BerSa001', 'Sara', 'Bertonassi', 1, '0000-00-00', '0000-00-00', 2, 1, 801, 'Sara.Bertonassi@Staples.com', '2013-03-01', '2013-03-28 14:57:42', 873, '2014-01-24 16:02:19', 873),
(1223, 'MerCo001', 'Corrie', 'Mercer', 1, '0000-00-00', '0000-00-00', 2, 1, 1243, 'Corrie.Mercer@Staples.com', '2013-04-15', '2013-04-11 10:21:20', 873, '2014-01-24 16:02:19', 873),
(1224, 'lindagonnella', 'Linda', 'Gonnella', 2, '0000-00-00', '0000-00-00', 2, 3, 731, 'Linda.Gonnella@quill.com', '2013-04-13', '2013-04-11 10:23:41', 873, '2014-01-24 14:09:18', 873),
(1225, 'kabvi001', 'Violet', 'Kabaso', 2, '0000-00-00', '0000-00-00', 2, 1, 767, 'Violet.Kabaso@Staples.com', '2013-04-15', '2013-04-16 14:01:06', 745, '2014-01-24 14:09:18', 745),
(1226, 'andreadeets', 'Andrea', 'Deets', 1, '0000-00-00', '0000-00-00', 2, 3, 1234, 'andrea.deets@quill.com', '2013-04-22', '2013-04-25 09:51:36', 873, '2014-01-24 16:02:19', 873),
(1227, 'RouTa001', 'Taryn', 'Roussel', 1, '0000-00-00', '0000-00-00', 2, 1, 1243, 'Taryn.Roussel@Staples.com', '2013-04-29', '2013-04-25 10:26:53', 873, '2014-01-24 16:02:19', 873),
(1228, 'SpeMa001', 'Mallory', 'Spector', 1, '0000-00-00', '0000-00-00', 1, 1, 777, 'Mallory.Spector@Staples.com', '2013-05-02', '2013-04-25 10:29:07', 873, '2014-01-24 14:09:18', 873),
(1229, 'WilBr007', 'Brian', 'Wilkins', 1, '0000-00-00', '0000-00-00', 7, 1, 756, 'Brian.Wilkins@Staples.com', '2013-04-22', '2013-04-29 15:11:20', 873, '2014-06-06 10:06:07', 873),
(1230, 'GagDe001', 'Deborah', 'Gagnon', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'Deborah.Gagnon@Staples.com', '2013-05-09', '2013-05-09 21:52:55', 873, '2014-07-10 16:54:06', 873),
(1231, 'FarEl001', 'Elizabeth', 'Farley', 2, '0000-00-00', '0000-00-00', 4, 1, 801, 'Elizabeth.Farley@Staples.com', '2013-04-29', '2013-04-15 15:43:55', 745, '2014-01-24 14:09:18', 745),
(1232, 'RugCh002', 'Christina', 'Ruggieri', 2, '0000-00-00', '0000-00-00', 2, 1, 781, 'Christina.Ruggieri@Staples.com', '2013-06-08', '2013-06-05 12:29:24', 873, '2014-01-24 14:09:18', 873),
(1233, 'chopa001', 'Patricia', 'Cho', 1, '0000-00-00', '0000-00-00', 6, 1, 1135, 'patricia.cho@staples.com', '2013-06-08', '2013-06-05 12:33:05', 745, '2014-01-24 16:07:29', 745),
(1234, 'sharonburger', 'Sharon', 'Burger', 2, '0000-00-00', '0000-00-00', 2, 3, 731, 'Sharon.Burger@quill.com', '2013-06-03', '2013-06-07 13:33:19', 745, '2014-06-03 12:13:54', 745),
(1235, 'BusKa002', 'Karle', 'Busse', 2, '0000-00-00', '0000-00-00', 1, 1, 837, 'Karle.Busse@Staples.com', '2013-06-10', '2013-06-10 15:50:56', 873, '2014-01-24 14:09:18', 873),
(1236, 'IngMe001', 'Meghan', 'Ingram', 1, '0000-00-00', '0000-00-00', 2, 1, 731, 'Meghan.Ingram@Staples.com', '2013-06-12', '2013-06-12 11:49:35', 873, '2014-01-24 16:02:19', 873),
(1237, 'morla001', 'Laurent', 'Morcrette', 1, '0000-00-00', '0000-00-00', 1, 2, 1084, 'laurent.morcrette@staples.ca', '2013-06-17', '2013-06-17 10:04:23', 873, '2014-01-24 14:09:18', 873),
(1238, 'JurHa001', 'Hannah', 'Jurist-Schoen', 2, '0000-00-00', '0000-00-00', 1, 1, 837, 'Hannah.Jurist-Schoen@Staples.com', '2013-06-10', '2013-06-20 14:40:04', 873, '2014-01-24 14:09:18', 873),
(1239, 'MarNa004', 'Nathan', 'Marholz', 2, '0000-00-00', '0000-00-00', 5, 1, 848, 'Nathan.Marholz@Staples.com', '2013-06-24', '2013-06-25 10:29:27', 873, '2014-01-24 14:09:18', 873),
(1240, 'MakJi001', 'Jillian', 'Makos', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'Jillian.Makos@Staples.com', '2013-06-25', '2013-06-25 10:31:28', 745, '2014-04-14 14:00:05', 745),
(1241, 'wyaja001', 'Jacklyn', 'Wyatt', 1, '0000-00-00', '0000-00-00', 7, 2, 1086, 'jacklyn.wyatt@staples.ca', '2013-06-25', '2013-06-25 13:19:18', 873, '2014-01-24 14:09:18', 873),
(1242, 'joelilek', 'Joe', 'Lilek', 1, '0000-00-00', '0000-00-00', 1, 3, 1167, 'Joe.Lilek@quill.com', '2013-07-08', '2013-07-08 16:38:56', 873, '2014-01-24 14:09:18', 873),
(1243, 'KelMa001', 'Maryhelen', 'Kelley', 1, '0000-00-00', '0000-00-00', 2, 1, 1233, 'Maryhelen.Kelley@Staples.com', '2013-07-22', '2013-07-24 15:23:28', 873, '2014-01-24 16:05:14', 873),
(1244, 'KamJo003', 'Joel', 'Kamm', 2, '0000-00-00', '0000-00-00', 7, 1, 847, 'Joel.Kamm@Staples.com', '2013-08-26', '2013-08-05 10:36:31', 745, '2014-01-24 14:09:18', 745),
(1245, 'BerJo002', 'Joseph', 'Bernasconi', 1, '0000-00-00', '0000-00-00', 7, 1, 1246, 'Joseph.Bernasconi@Staples.com', '2013-08-05', '2013-08-05 10:38:52', 873, '2014-01-24 14:09:18', 873),
(1246, 'yemha001', 'Hakki', 'Yemeniciler', 1, '0000-00-00', '0000-00-00', 6, 1, 847, 'hakki.yemeniciler@staples.com', '2013-08-01', '2013-08-05 10:40:14', 745, '2014-04-14 10:17:46', 745),
(1247, 'QuiMi002', 'Michael', 'Quiet', 1, '0000-00-00', '0000-00-00', 1, 1, 727, 'Michael.Quiet@Staples.com', '2013-08-10', '2013-08-05 10:42:51', 745, '2014-04-21 12:53:25', 745),
(1248, 'MayNa002', 'Nancy', 'Maya', 1, '0000-00-00', '0000-00-00', 7, 1, 756, 'Nancy.Maya@Staples.com', '2013-08-19', '2013-08-05 10:47:44', 873, '2014-01-24 14:09:18', 873),
(1249, 'justinrunyard', 'Justin', 'Runyard', 1, '0000-00-00', '0000-00-00', 1, 3, 1186, 'Justin.Runyard@quill.com', '2013-08-05', '2013-08-05 13:27:57', 873, '2014-01-24 14:09:18', 873),
(1250, 'CudTi001', 'Tiffany', 'Cuddihy', 1, '0000-00-00', '0000-00-00', 5, 1, 848, 'Tiffany.Cuddihy@Staples.com', '2013-08-19', '2013-08-19 13:38:18', 873, '2014-01-24 14:09:18', 873),
(1251, 'DeSMi002', 'Michael', 'DeStoop', 1, '0000-00-00', '0000-00-00', 8, 1, 815, 'Michael.DeStoop@Staples.com', '2013-09-03', '2013-08-19 13:41:07', 873, '2014-01-24 14:09:18', 873),
(1252, 'LynEl001', 'Elizabeth', 'Lynn', 1, '0000-00-00', '0000-00-00', 4, 1, 799, 'Elizabeth.Lynn@Staples.com', '2013-09-03', '2013-08-19 13:46:11', 873, '2014-01-24 16:06:17', 873),
(1253, 'FurSc001', 'Scott', 'Furlong', 2, '0000-00-00', '0000-00-00', 7, 1, 756, 'Scott.Furlong@Staples.com', '2013-08-12', '2013-08-19 13:53:54', 745, '2014-04-14 14:02:29', 745),
(1254, 'kiekr001', 'Kris', 'Kiehn', 1, '0000-00-00', '0000-00-00', 2, 1, 731, 'kris.kiehn@staples.com', '2013-08-19', '2013-08-19 13:58:16', 864, '2014-01-24 16:02:19', 864),
(1255, 'SpaSt002', 'Stefanie', 'Sparrow', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'Stefanie.Sparrow@Staples.com', '2013-08-19', '2013-08-26 14:13:18', 745, '2014-03-17 16:01:52', 745),
(1256, 'wilka001', 'Katherine', 'Wilson', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'katherine.wilson@staples.com', '2013-09-03', '2013-09-05 12:14:41', 745, '2014-04-14 14:00:17', 745),
(1257, 'McKPh001', 'Phyllis', 'McKee', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Phyllis.McKee@Staples.com', '2013-08-27', '2013-09-05 12:32:05', 745, '2014-03-05 16:45:20', 745),
(1258, 'LanEl001', 'Ellen', 'Lane', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Ellen.Lane@Staples.com', '2013-08-27', '2013-09-05 12:37:48', 745, '2014-03-05 16:44:17', 745),
(1259, 'crage001', 'Genevieve', 'Crane', 2, '0000-00-00', '0000-00-00', 1, 1, 796, 'genevieve.crane@staples.com', '2013-09-07', '2013-09-06 16:25:42', 745, '2014-04-21 12:44:38', 745),
(1260, 'johma007', 'Marta', 'Johnson', 1, '0000-00-00', '0000-00-00', 7, 1, 879, 'marta.johnson@staples.com', '2013-09-09', '2013-09-19 17:04:04', 873, '2014-01-24 14:09:18', 873),
(1261, 'dieda001', 'Darryl', 'Dieudonne', 1, '0000-00-00', '0000-00-00', 5, 1, 765, 'darryl.dieudonne@staples.com', '2013-09-19', '2013-09-20 16:54:09', 873, '2014-04-09 16:41:20', 873),
(1262, 'BowSt003', 'Steve', 'Bowles', 2, '0000-00-00', '0000-00-00', 4, 1, 831, 'Steve.Bowles@Staples.com', '2013-09-30', '2013-10-02 14:17:05', 745, '2014-03-05 16:45:02', 745),
(1263, 'DreAr001', 'Ariel', 'Dreyer', 1, '0000-00-00', '0000-00-00', 5, 1, 765, 'Ariel.Dreyer@Staples.com', '2013-10-02', '2013-10-02 14:21:24', 743, '2014-01-24 14:09:18', 743),
(1264, 'kimhy001', 'Hye', 'Kim', 1, '0000-00-00', '0000-00-00', 7, 1, 1246, 'HyeMi.Kim@Staples.com', '2013-10-01', '2013-10-02 14:26:27', 873, '2014-01-24 14:09:18', 873),
(1266, 'KeaJe002', 'Jennifer', 'Keany', 1, '0000-00-00', '0000-00-00', 11, 1, 727, 'Jennifer.Keany@Staples.com', '2013-11-04', '2013-11-05 10:49:03', 873, '2014-01-24 14:09:18', 873),
(1267, 'scottmies', 'Scott', 'Mies', 1, '0000-00-00', '0000-00-00', 12, 3, 1267, 'scott.mies@quill.com', '2013-11-16', '2013-11-11 12:18:58', 1191, '2014-01-24 14:09:18', 1191),
(1268, 'bolst001', 'Stephanie', 'Bolio', 1, '0000-00-00', '0000-00-00', 2, 1, 731, 'Stephanie.Bolio@Staples.com', '2013-09-16', '2013-11-11 12:52:55', 873, '2014-01-24 16:02:19', 873),
(1269, 'cherielufman', 'Cherie', 'Lufman', 1, '0000-00-00', '0000-00-00', 12, 3, 1267, 'cherie.lufman@quill.com', '2013-11-16', '2013-11-11 13:07:46', 1267, '2014-01-24 14:09:18', 1267),
(1270, 'dhvanipatel', 'Dhvani', 'Patel', 1, '0000-00-00', '0000-00-00', 12, 3, 1269, 'dhvani.patel@quill.com', '2013-11-16', '2013-11-11 13:19:38', 873, '2014-01-24 14:09:18', 873),
(1271, 'wahabmian', 'Wahab', 'Mian', 1, '0000-00-00', '0000-00-00', 12, 3, 1269, 'wahab.mian@quill.com', '2013-11-16', '2013-11-11 13:23:00', 1267, '2014-01-24 14:09:18', 1267),
(1272, 'bruceziegler', 'Bruce', 'Ziegler', 1, '0000-00-00', '0000-00-00', 12, 3, 1267, 'bruce.ziegler@quill.com', '2013-11-16', '2013-11-11 13:23:59', 1267, '2014-01-24 14:09:18', 1267),
(1273, 'angelamartino', 'Angela', 'Martino', 1, '0000-00-00', '0000-00-00', 12, 3, 1272, 'angela.martino@quill.com', '2013-11-16', '2013-11-11 13:24:56', 1267, '2014-01-24 14:09:18', 1267),
(1274, 'maggiewestley', 'Maggie', 'Westley', 1, '0000-00-00', '0000-00-00', 12, 3, 1272, 'maggie.westley@quill.com', '2013-11-16', '2013-11-11 13:25:53', 1267, '2014-01-24 14:09:18', 1267),
(1275, 'pathink', 'Pat', 'Hink', 1, '0000-00-00', '0000-00-00', 12, 3, 1272, 'pat.hink@quill.com', '2013-11-16', '2013-11-11 13:26:54', 1267, '2014-01-24 14:09:18', 1267),
(1276, 'kalechmathieu', 'Kalech', 'Mathieu', 1, '0000-00-00', '0000-00-00', 12, 3, 1272, 'kalech.mathieu@quill.com', '2013-11-16', '2013-11-11 13:27:55', 1267, '2014-01-24 14:09:18', 1267),
(1277, 'KinGe001', 'Geoff', 'King', 1, '0000-00-00', '0000-00-00', 7, 1, 722, 'Geoff.King@Staples.com', '2013-11-18', '2013-11-14 09:46:01', 873, '2014-01-24 14:09:18', 873),
(1278, 'colho001', 'Holly', 'Colby', 1, '0000-00-00', '0000-00-00', 2, 1, 1243, 'Holly.Colby@Staples.com', '2013-11-19', '2013-11-19 11:14:39', 873, '2014-01-24 16:02:19', 873),
(1279, 'walkr001', 'Kristian', 'Walker', 2, '0000-00-00', '0000-00-00', 2, 1, 767, 'kristian.walker@staples.com', '2013-11-20', '2013-11-20 09:54:06', 873, '2014-03-03 14:21:02', 873),
(1280, 'natashiabryson', 'Natashia', 'Bryson', 1, '0000-00-00', '0000-00-00', 1, 3, NULL, 'Natashia.Bryson@quill.com', '2013-12-09', '2013-11-22 16:34:52', 1191, '2014-01-24 14:09:18', 1191),
(1281, 'keeer002', 'Erin', 'Keenan', 2, '0000-00-00', '0000-00-00', 2, 1, 976, 'Erin.Keenan@Staples.com', '2013-11-25', '2013-11-26 08:59:07', 745, '2014-04-14 13:54:03', 745),
(1282, 'denma001', 'Meghan', 'Dente', 2, '0000-00-00', '0000-00-00', 5, 1, 728, 'Meghan.Dente@Staples.com', '2013-12-05', '2013-12-10 14:37:30', 745, '2014-04-14 13:57:09', 745),
(1283, 'kenmi001', 'Michael', 'Kent', 1, '0000-00-00', '0000-00-00', 2, 1, 781, 'michael.kent2@staples.com', '2014-01-06', '2014-01-06 11:49:37', 873, '2014-01-24 16:02:19', 873),
(1284, 'DreAr001', 'Ariel', 'Dreyer', 2, '0000-00-00', '0000-00-00', 1, 1, 765, 'Ariel.Dreyer@Staples.com', '2014-01-13', '2014-01-13 11:47:06', 873, '2014-01-24 14:09:18', 873),
(1285, 'conch002', 'Christine', 'Conway', 1, '0000-00-00', '0000-00-00', 1, 1, 765, 'christine.conway@Staples.com', '2014-01-09', '2014-01-13 11:52:59', 873, '2014-06-09 14:59:20', 873),
(1286, 'PapKa002', 'Popi', 'Papadonta', 1, '0000-00-00', '0000-00-00', 4, 1, 825, 'Popi.Papadonta@staples.com', '2014-01-22', '2014-01-22 10:23:41', 873, '2014-01-24 16:06:17', 873),
(1287, 'davda002', 'David', 'Davis', 1, '0000-00-00', '0000-00-00', 1, 1, 727, 'david.davis@staples.com', '2014-01-13', '2014-01-31 11:03:15', 873, '2014-05-13 11:49:15', 873),
(1288, 'kristenhenmueller', 'Kristen', 'Henmueller', 1, '0000-00-00', '0000-00-00', 1, 3, 735, 'kristen.henmueller@quill.com', '2014-03-08', '2014-03-03 17:17:13', 1191, '2014-05-13 11:49:17', 1191),
(1289, 'svickers', 'Sarah', 'Vickers', 1, '0000-00-00', '0000-00-00', 6, 2, NULL, 'Sarah.Vickers@staples.ca', '2014-04-01', '2014-04-01 10:18:43', 873, '2014-05-13 11:49:19', 873),
(1290, 'walwe002', 'Wendy', 'Walsh', 1, '0000-00-00', '0000-00-00', 7, 1, 879, 'Wendy.Walsh@Staples.com', '2014-04-09', '2014-04-15 08:48:20', 873, '2014-05-13 11:49:20', 873),
(1291, 'HamRo001', 'Robert', 'Hamilton', 1, '0000-00-00', '0000-00-00', 1, 1, 728, 'Robert.Hamilton@Staples.com', '2014-04-14', '2014-04-15 08:50:03', 873, '2014-05-13 11:49:22', 873),
(1292, 'KilGe001', 'George', 'Kilmain', 1, '0000-00-00', '0000-00-00', 7, 1, 879, 'George.Kilmain@Staples.com', '2014-04-22', '2014-04-23 16:12:39', 873, '2014-05-13 11:49:25', 873),
(1293, 'webpa002', 'Paul', 'Weber', 1, '0000-00-00', '0000-00-00', 7, 1, 879, 'Paul.Weber@Staples.com', '2014-04-29', '2014-04-30 11:34:12', 873, '2014-05-13 11:49:13', 873),
(1294, 'dontinawalsten', 'Dontina', 'Walsten', 1, '0000-00-00', '0000-00-00', 4, 3, 1198, 'dontina.walsten@quill.com', '2014-05-03', '2014-05-01 10:21:07', 1191, '2014-05-13 11:49:10', 1191),
(1295, 'MetJo001', 'Joseph', 'Metcalfe', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'Joseph.Metcalfe@Staples.com', '2014-05-05', '2014-05-01 17:14:11', 873, '2014-05-13 11:49:27', 873);
INSERT INTO `users` (`user_id`, `username`, `first_name`, `last_name`, `status_id`, `status_begin_date`, `status_end_date`, `team_id`, `location_id`, `manager_id`, `email`, `start_date`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1296, 'ZouYi001', 'Yi', 'Zou', 1, '0000-00-00', '0000-00-00', 4, 1, 831, 'Yi.Zou@Staples.com', '2014-05-05', '2014-05-01 17:15:28', 873, '2014-05-13 11:49:29', 873),
(1297, 'ShaRa006', 'Rachel', 'Shabani', 1, '0000-00-00', '0000-00-00', 1, 1, 765, 'Rachel.Shabani@Staples.com', '2014-05-05', '2014-05-01 17:18:15', 873, '2014-05-13 11:49:31', 873),
(1298, 'HarAl001', 'Ali', 'Harkavy', 1, '0000-00-00', '0000-00-00', 4, 1, 801, 'Ali.Harkavy@Staples.com', '2014-05-30', '2014-05-01 17:19:46', 745, '2014-06-03 12:13:17', 745),
(1299, 'tavel001', 'Eliza', 'Tavares', 1, '0000-00-00', '0000-00-00', 2, 1, 767, 'Eliza.Tavares@staples.com', '2014-05-07', '2014-05-06 15:30:00', 873, '2014-07-30 17:13:16', 873),
(1300, 'JicJa001', 'Jad', 'Jichi', 1, '0000-00-00', '0000-00-00', 5, 1, 735, 'Jad.Jichi@Staples.com', '2014-05-17', '2014-05-15 15:00:00', 873, '2014-05-15 15:00:00', 873),
(1301, 'ChePo001', 'Polly', 'Chevalier', 1, '0000-00-00', '0000-00-00', 9, 1, 728, 'Polly.Chevalier@Staples.com', '2014-05-19', '2014-05-19 11:48:09', 873, '2014-05-29 12:56:43', 873),
(1302, 'bakki001', 'Kirsten', 'Bakstran', 1, '0000-00-00', '0000-00-00', 2, 1, 1112, 'kirsten.bakstran@staples.com', '2014-06-03', '2014-06-03 12:25:32', 873, '2014-06-03 12:25:32', 873),
(1303, 'tibmi001', 'Michael', 'Tibbals', 1, '0000-00-00', '0000-00-00', 1, 1, 722, 'michael.tibbals@staples.com', '2014-06-02', '2014-06-05 11:49:01', 873, '2014-06-05 11:49:01', 873),
(1304, 'DeWDa001', 'Dan', 'DeWalt', 1, '0000-00-00', '0000-00-00', 6, 1, 1289, 'Dan.DeWalt@Staples.com', '2014-06-05', '2014-06-05 11:54:43', 873, '2014-06-13 15:42:10', 873),
(1305, 'angelaquinlan', 'Angela', 'Quinlan', 1, '0000-00-00', '0000-00-00', 2, 3, 1175, 'angela.quinlan@quill.com', '2014-06-14', '2014-06-12 17:18:37', 1191, '2014-06-12 17:19:11', 1191),
(1306, 'BenAl001', 'Alyssa', 'Benefit', 1, '0000-00-00', '0000-00-00', 2, 1, 781, 'Alyssa.Benefit@Staples.com', '2014-06-16', '2014-06-13 14:09:44', 873, '2014-06-13 14:09:44', 873),
(1307, 'fueja001', 'Javier', 'Fuentes', 1, '0000-00-00', '0000-00-00', 1, 1, 765, 'javier.fuentes@staples.com', '2014-06-30', '2014-07-01 11:31:14', 873, '2014-07-01 11:31:14', 873),
(1308, 'holry002', 'Ryan', 'Holt', 3, '2014-08-14', '2014-08-29', 3, 1, 872, 'ryan.holt@staples.com', '2014-07-09', '2014-07-09 10:34:44', 873, '2014-08-15 10:51:28', 1308),
(1309, 'samsh001', 'Shikarro', 'Egan', 1, '0000-00-00', '0000-00-00', 2, 1, 1233, 'shikarro.sampsonegan@staples.com', '2014-07-14', '2014-07-14 12:12:25', 873, '2014-07-14 12:15:38', 873);

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_view`
--
CREATE TABLE IF NOT EXISTS `users_view` (
`user_id` int(10) unsigned
,`username` varchar(60)
,`name` varchar(401)
,`email` varchar(200)
,`start_date` date
,`manager` varchar(401)
,`status_id` int(12)
,`status` varchar(200)
,`team` varchar(200)
,`location` varchar(200)
);
-- --------------------------------------------------------

--
-- Table structure for table `user_applications`
--

CREATE TABLE IF NOT EXISTS `user_applications` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `application_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `user_applications`
--

INSERT INTO `user_applications` (`id`, `user_id`, `application_id`) VALUES
(29, 1308, 2),
(28, 1308, 1),
(7, 873, 1),
(8, 873, 2),
(9, 1119, 1),
(10, 1119, 2),
(11, 745, 1),
(12, 745, 2),
(33, 872, 2),
(32, 872, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `role_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_id`, `role_id`) VALUES
(79, 873, 1),
(80, 873, 2),
(81, 873, 3),
(82, 873, 4),
(83, 1119, 1),
(84, 1119, 2),
(85, 1119, 3),
(86, 1119, 4),
(87, 745, 1),
(88, 745, 2),
(89, 745, 3),
(90, 745, 4),
(119, 1308, 1),
(120, 1308, 2),
(121, 1308, 3),
(126, 872, 1),
(127, 872, 2),
(128, 872, 4);

-- --------------------------------------------------------

--
-- Structure for view `users_view`
--
DROP TABLE IF EXISTS `users_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_view` AS select `e`.`user_id` AS `user_id`,`e`.`username` AS `username`,concat(convert(`e`.`first_name` using utf8),' ',`e`.`last_name`) AS `name`,`e`.`email` AS `email`,`e`.`start_date` AS `start_date`,concat(convert(`m`.`first_name` using utf8),' ',`m`.`last_name`) AS `manager`,`s`.`status_id` AS `status_id`,`s`.`status` AS `status`,`t`.`team` AS `team`,`l`.`location` AS `location` from ((((`users` `e` join `users` `m` on((`e`.`manager_id` = `m`.`user_id`))) left join `statuses` `s` on((`e`.`status_id` = `s`.`status_id`))) left join `teams` `t` on((`e`.`team_id` = `t`.`team_id`))) left join `locations` `l` on((`e`.`location_id` = `l`.`location_id`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
