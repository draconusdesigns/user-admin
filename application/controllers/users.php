<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller{

    function __construct(){
        parent::__construct();

        $this->load->model('user_model');
		$this->load->model('location_model');
		$this->load->model('team_model');
		$this->load->model('status_model');
		$this->load->model('application_model');
		
		$this->user_search_terms = $this->session->userdata('user_search_terms');
    }

    function index(){
        $this->is_logged_in(); //If not logged in, redirect to login

        $data['titleTag'] = "Users";
        $data['pageHeading'] = "<i class='fa fa-users'></i> Users";
		$data['user_search_terms'] = $this->user_search_terms;

		$config['base_url'] = base_url().'/users/index/';
		$config['total_rows'] = $this->user_model->countUsers($this->user_search_terms);
		$config['per_page'] = 25;
		$this->pagination->initialize($config);
		
		$data['users'] = $this->user_model->getPaginatedUsers($config['per_page'], $this->uri->segment(3), $this->user_search_terms);	
		$data['links'] = $this->pagination->create_links();

		$data['statuses'] = $this->status_model->getStatusDropdown();
		
		//Get all Roles for All Users
		
		foreach ($data['users'] as $k => $user){
			$data['users'][$k]->roles = $this->role_model->getUserRolesArray($user->user_id);
		}
		
		//Get all Applications for All Users
		
		foreach ($data['users'] as $k => $user){
			$data['users'][$k]->applications = $this->application_model->getUserApplicationsArray($user->user_id);
		}
		
        $this->load->view('template/header', $data);
        $this->load->view('users/index', $data);
      	$this->load->view('template/footer', $data);
    }

	function search(){
		//Get keys and values and pass them into the createSearch Array function
		$terms = $this->input->post('terms');
		$user_terms = $this->user_search_terms;
		$user_terms[] = str_replace('"', '', $terms);
		$this->session->set_userdata('user_search_terms', $user_terms);
		$this->json(array('response'=>'success'));
	}
	
	function remove_term($term=null) {
		$term = $this->input->post('term') ? $this->input->post('term') : $term;
		$user_terms = $this->user_search_terms;
		$i = 0;
		foreach ( $user_terms as $user_term ) {
			if ( $user_term == $term ) {
				unset($user_terms[$i]);
			}
			$i++;
		}
		$this->session->set_userdata('user_search_terms', $user_terms);
		$this->json(array('response'=>'success'));
	}
	
	function search_reset() { 
		$this->session->set_userdata('user_search_terms', NULL);
		$this->session->unset_userdata('user_search_terms');
		redirect('users/index', 'location');
	}
	
    public function add() {
        $this->is_logged_in(); //If not logged in, redirect to login

        $data['titleTag'] = 'Add a User';
        $data['pageHeading'] = "<i class='fa fa-user'></i> Add a User";
        $data['pageSubHeading'] = "";

		$data['managers'] = $this->user_model->getManagerDropdown();
		$data['locations'] = $this->location_model->getLocationDropdown();
		$data['teams'] = $this->team_model->getTeamDropdown();
		$data['statuses'] = $this->status_model->getStatusDropdown();
		$data['roles'] = $this->role_model->getRoles();
		$data['applications'] = $this->application_model->getApplications();
		
        if ($this->form_validation->run('user_validation') == FALSE){
            $this->load->view('template/header', $data);
            $this->load->view('users/update', $data);
	      	$this->load->view('template/footer', $data);
		} else {
            //insert user
            $this->user_model->addUser($data);

            redirect('users', 'location');
        }
    }

    public function edit($user_id) {
        $this->is_logged_in(); //If not logged in, redirect to login

        $data['user'] = $this->user_model->getUser($user_id);
		$data['managers'] = $this->user_model->getManagerDropdown();
		$data['locations'] = $this->location_model->getLocationDropdown();
		$data['teams'] = $this->team_model->getTeamDropdown();
		$data['statuses'] = $this->status_model->getStatusDropdown();
		$data['roles'] = $this->role_model->getRoles();
		$data['applications'] = $this->application_model->getApplications();
		
		
		$data['user']->roles = $this->role_model->getUserRolesIDArray($user_id);
		$data['user']->applications = $this->application_model->getUserApplicationIDArray($user_id);
		
        $data['titleTag'] = "Editing {$data['user']->first_name} {$data['user']->last_name}";
        $data['pageHeading'] = "<i class='fa fa-user'></i>  {$data['user']->first_name} {$data['user']->last_name}";
        $data['pageSubHeading'] = "";

        if ($this->form_validation->run('user_validation') == FALSE){
            $this->load->view('template/header', $data);
            $this->load->view('users/update', $data);
	      	$this->load->view('template/footer', $data);
        } else {
            //update user
            $this->user_model->updateUser($data);

            redirect('users', 'location');
        }
    }

	function update_status($status_id, $user_id){
		$this->user_model->updateStatus($status_id, $user_id);
	}
	
	function login(){
		$this->load->library('ldap');
        $data['titleTag'] = "Login";
        $data['pageHeading'] = "Login";
        $data['pageSubHeading'] = "Please login with your username and password below.";

        if ($this->form_validation->run() == FALSE){
            $this->load->view('template/login_header', $data);
            $this->load->view('login', $data);
	      	$this->load->view('template/footer', $data);
        } else {
            //login
            $result = $this->ldap->validate($this->input->post('identity'), $this->input->post('password'));
			if($result == 'OK'){
            	$logged_in = $this->user_model->login();
			}else{
				$this->session->set_flashdata('error','Invalid Credentials');
				redirect('users/login', 'refresh');
			}
			
            if($logged_in) {
                redirect('http://advweb3.staples.com/walter', 'location');
            }else {
                redirect('users/login');
            }
        }

    }

    function logout(){
        $this->is_logged_in(); //If not logged in, redirect to login

        $this->session->sess_destroy();
		redirect('users/login', 'location');
    }
}