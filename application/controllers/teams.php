<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Teams extends MY_Controller{

    function __construct(){
        parent::__construct();

        $this->is_logged_in(); //If not logged in, redirect to login
        $this->is_of_role(1) || redirect('dashboard/index', 'refresh');

        $this->load->model('team_model');
    }

    function index(){
        $data['titleTag'] = "Teams";
        $data['pageHeading'] = "<i class='fa fa-comments-o'></i> Teams";

        $data['teams'] = $this->team_model->getTeams();

        $this->load->view('template/header', $data);
        $this->load->view('teams/index', $data);
      	$this->load->view('template/footer', $data);
    }

	function add() {				
        $data['titleTag'] = "Add Team";
		$data['pageHeading'] = "Add Team";
		$data['pageSubHeading'] = "";
				
		if ($this->form_validation->run('team_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('teams/update', $data);
	      	$this->load->view('template/footer', $data);		
		} else {
			//insert Team
			$this->team_model->addTeam($data);

			redirect('/teams/index', 'refresh');
		}
	}

	function edit($team_id) {	
		$data['team'] = $this->team_model->getTeam($team_id);
			
        $data['titleTag'] = "Edit {$data['team']->team} Team";
		$data['pageHeading'] = "Edit {$data['team']->team} Team";
		$data['pageSubHeading'] = "";

		if ($this->form_validation->run('team_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('teams/update', $data);
	      	$this->load->view('template/footer', $data);	
		} else {
			//update Team
			$this->team_model->updateTeam($data);
			
			redirect('teams/index', 'location');
		}
	}
}