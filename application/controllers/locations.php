<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends MY_Controller{

    function __construct(){
        parent::__construct();

        $this->is_logged_in(); //If not logged in, redirect to login
        $this->is_of_role(1) || redirect('dashboard/index', 'refresh');

        $this->load->model('location_model');
    }

    function index(){
        $data['titleTag'] = "Locations";
        $data['pageHeading'] = "<i class='fa fa-building'></i> Locations";

        $data['locations'] = $this->location_model->getLocations();

        $this->load->view('template/header', $data);
        $this->load->view('locations/index', $data);
      	$this->load->view('template/footer', $data);
    }

	function add() {				
		$data['titleTag'] = 'User Locations';
		$data['pageHeading'] = "Add Location";
		$data['pageSubHeading'] = "";
				
		if ($this->form_validation->run('location_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('locations/update', $data);
			$this->load->view('template/footer', $data);
		} else {
			//insert Location
			$this->location_model->addLocation($data);

			redirect('/locations/index', 'refresh');
		}
	}

	function edit($location_id) {	
		$data['location'] = $this->location_model->getLocation($location_id);
			
		$data['titleTag'] = "Edit {$data['location']->location} Location";
		$data['pageHeading'] = "Edit {$data['location']->location} Location";
		$data['pageSubHeading'] = "";

		if ($this->form_validation->run('location_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('locations/update', $data);
			$this->load->view('template/footer', $data);
		} else {
			//update Location
			$this->location_model->updateLocation($data);
			
			redirect('locations/index', 'location');
		}
	}
}