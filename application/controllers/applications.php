<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Applications extends MY_Controller{

    function __construct(){
        parent::__construct();

        $this->is_logged_in(); //If not logged in, redirect to login
        $this->is_of_role(1) || redirect('dashboard/index', 'refresh');

        $this->load->model('application_model');
    }

    function index(){
        $data['titleTag'] = "Applications";
        $data['pageHeading'] = "<i class='fa fa-laptop'></i> Applications";

        $data['applications'] = $this->application_model->getApplications();

        $this->load->view('template/header', $data);
        $this->load->view('applications/index', $data);
      	$this->load->view('template/footer', $data);
    }

	function add() {				
		$data['titleTag'] = 'Applications';
		$data['pageHeading'] = "Add Application";
		$data['pageSubHeading'] = "";
				
		if ($this->form_validation->run('application_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('applications/update', $data);
			$this->load->view('template/footer', $data);
		} else {
			//insert application
			$this->application_model->addApplication($data);

			redirect('/applications/index', 'refresh');
		}
	}

	function edit($application_id) {	
		$data['application'] = $this->location_model->getApplication($application_id);
			
		$data['titleTag'] = "Edit {$data['application']->application} Application";
		$data['pageHeading'] = "Edit {$data['application']->application} Application";
		$data['pageSubHeading'] = "";

		if ($this->form_validation->run('application_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('applications/update', $data);
			$this->load->view('template/footer', $data);
		} else {
			//update Application
			$this->application_model->updateApplication($data);
			
			redirect('applications/index', 'location');
		}
	}
}