<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Roles extends MY_Controller{

    function __construct(){
        parent::__construct();

	    $this->is_logged_in(); //If not logged in, redirect to login
        $this->is_of_role(1) || redirect('dashboard/index', 'refresh');

        $this->load->model('role_model');
    }

    function index(){
        $data['titleTag'] = "Roles";
        $data['pageHeading'] = "<i class='fa fa-cubes'></i> Roles";

        $data['roles'] = $this->role_model->getRoles();

        $this->load->view('template/header', $data);
        $this->load->view('roles/index', $data);
      	$this->load->view('template/footer', $data);
    }

	function add() {				
		$data['titleTag'] = 'User Roles';
		$data['pageHeading'] = "Add User Role";
		$data['pageSubHeading'] = "";
				
		if ($this->form_validation->run('role_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('roles/update', $data);
			$this->load->view('template/footer', $data);
		} else {
			//insert Role
			$this->role_model->addRole($data);

			redirect('/roles/index', 'refresh');
		}
	}

	function edit($id) {	
		$data['role'] = $this->role_model->getRole($id);
			
        $data['titleTag'] = "Edit {$data['role']->role} Role";
		$data['pageHeading'] = "Edit {$data['role']->role} Role";
		$data['pageSubHeading'] = "";

		if ($this->form_validation->run('role_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('roles/update', $data);
			$this->load->view('template/footer', $data);
		} else {
			//update Role
			$this->role_model->editRole($data);
			
			redirect('roles/index', 'location');
		}
	}
}