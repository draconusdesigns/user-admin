<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

   function __construct() {
      parent::__construct();

       $this->is_logged_in(); //If not logged in, redirect to login
   }

   public function index() {
	  $data['titleTag'] = "Home";	
      $data['pageHeading'] = "Welcome to Lexi";
      $data['pageSubHeading'] = "The Agency User Lexicon";

      $this->load->view('template/header', $data);
      $this->load->view('index', $data);
      $this->load->view('template/footer', $data);
   }

}