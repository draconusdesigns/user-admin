<?php if(validation_errors()):?>
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <?= validation_errors() ?>
	</div>
<?php endif;?>

<form class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="role_id" value="<?= isset($role->role_id) ? $role->role_id : set_value("role_id") ?>" />
	
	<div class="form-group">
		<label for="role" class="col-md-3 control-label">Role Name: </label>
		<div class="col-md-5">
			<input type="text" name="role" class="input-xxlarge form-control" value="<?= isset($role->role) ? $role->role : set_value("role") ?>"/>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="description" class="col-md-3 control-label">Description: </label>
		<div class="col-md-5">
			<textarea class="text-left" rows="2" cols="54" type="text" name="description" id="description"><?= isset($role->description) ? $role->description : set_value("description") ?></textarea>
		</div>	
	</div>
	
	<div class="col-xs-3 col-sm-1 col-md-4 col-md-offset-4">
		<input class="btn btn-primary pull-right" type="submit" value="submit" />
	</div>
</form>