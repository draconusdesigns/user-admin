<div class="row">
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>roles/add" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa fa-plus-circle'></i> Add a New Role
		</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
        <table class="table table-striped tablesorter">
            <thead class="thead">
			<th class="center">Role ID</th>
            <th>Role Name</th>
            <th>Role Description</th>
            <td class="center"><i class="fa fa-edit"></i></td>
            </thead>
            <tbody>
            <?php foreach ($roles as $role):?>
                <tr>
	                <td class="center"><?= $role->role_id ?></td>
                    <td><?= $role->role ?></td>
                    <td><?= $role->description ?></td>
                    <td class="center"><?= anchor("roles/edit/".$role->role_id, 'Edit') ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div><!-- End #queue -->
</div><!-- End .row -->