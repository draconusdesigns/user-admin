<div class="row">
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>applications/add" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa fa-plus-circle'></i> Add a New Application
		</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
        <table class="table table-striped tablesorter">
            <thead class="thead">
			<th class="center">Application ID</th>
            <th>Application Name</th>
            <th>Application Description</th>
            <td class="center"><i class="fa fa-edit"></i></td>
            </thead>
            <tbody>
            <?php foreach ($applications as $application):?>
                <tr>
	                <td class="center"><?= $application->application_id ?></td>
                    <td><?= $application->application ?></td>
                    <td><?= $application->description ?></td>
                    <td class="center"><?= anchor("roles/edit/".$application->application_id, 'Edit') ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div><!-- End #queue -->
</div><!-- End .row -->