	</div>
</div>
<footer class="hidden-print">
	<div class="container">
	<div class="row">
		<div class="col-md-3">
			<img src="<?= asset_url() ?>/images/agency_worldwide.png" alt="agency_worldwide" width="200" />
		</div>
	
		<div class="col-md-9">
			<p class="text-center">Copyright &copy; <span id="copyright_year"><?= date('Y') ?></span> <a href="mailto:agencyops@staples.com" title="Agency Operations Department">Agency Operations</a> Staples, Inc. All Rights Reserved.</p>
		</div>
	
	</div>

	
	</div>
</footer>
</body>
</html>