<?php if(validation_errors()):?>
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <?= validation_errors() ?>
	</div>
<?php endif;?>

<div class="row">
	<div class="col-md-4">
		<form method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="identity">Username: </label>
				<input type="text" name="identity" class="input-xxlarge form-control" value=""/>
			</div>
	
			<div class="form-group">
				<label for="password">Password: </label>
				<input type="password" name="password" class="input-xxlarge form-control" value=""/>
			</div>
	
			<input class="btn btn-default pull-right" type="submit" value="Login" />
		</form>
	</div>
</div>