<div class="row">
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>teams/add" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa fa-plus-circle'></i> Add a New Team
		</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
        <table class="table table-striped tablesorter">
            <thead class="thead">
            <th>Team Name</th>
            <th>Team Description</th>
            <td class="center"><i class="fa fa-edit"></i></td>
            </thead>
            <tbody>
            <?php foreach ($teams as $team):?>
                <tr>
                    <td><?= $team->team ?></td>
                    <td><?= $team->description ?></td>
                    <td class="center"><?= anchor("teams/edit/".$team->team_id, 'Edit') ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div><!-- End #queue -->
</div><!-- End .row -->