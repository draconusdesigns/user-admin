<script type="text/javascript" src="<?= asset_url() ?>/js/usersearch.js"></script>

<div class="row">
	<div class="col-md-8">
	    <form class="form-block" name="last_timesheet_selector" id="last_timesheet_selector" action="">
	        <select name="search-term" id="search-term" class="form-control pull-left" style="width:auto;margin-right:10px;">
	            <option value='all'>All</option>
	        </select>
	        <input type="text" name="search-terms" id="search-terms" placeholder="Search Terms" autocomplete="off" class="form-control pull-left" style="width:250px;" value=""/>
	        <button class="btn-primary form-control pull-left" id="search-btn" style="width:auto;margin-left:10px;">Search</button>
	        <img src="<?= base_url() ?>assets/css/ajax-loader.gif" style="margin-left:10px;margin-top:16px;display:none;" id="loader-icon" />
	    </form>
			<a style="text-decoration: none;" href="<?= base_url() ?>users/search_reset">
				<button class="btn-danger btn-xs pull-left" style="width:auto;margin-left:10px;">Reset Search</button>
			</a>
	</div>
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>users/add" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa fa-plus-circle'></i> Add a New User
		</a>
	</div>
</div>
<?php if ($user_search_terms): ?>
	<?php foreach ( $user_search_terms as $term ): ?>
		<?php
			$xS = explode(':', $term);
			$keys = $xS[0];
			$vals = $xS[1];
		?>
		<div class='alert alert-info alert-dismissible filter pull-left' role='alert'>
			<button type='button' class='close rem-term' data-dismiss='alert' data-rel="<?= $term ?>">
				<span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span>
			</button>
			<strong><?= ucwords($keys) ?></strong> : <?= $vals ?>
		</div>
	<?php endforeach; ?>
<?php endif;?>
<div class="row">
	<div class="col-md-12">
	    <table id="search-results" class="table table-striped tablesorter">
            <thead class="thead">
            <th>Name</th>
			<th>Location</th>
			<th>Team</th>
			<th>Manager</th>
			<td>Roles</td>
			<td>App Access</td>
            <th class="center">Status</th>
            <td class="center"><i class="fa fa-edit"></i></td>
            </thead>
            <tbody>
            <?php foreach ($users as $user):?>
                <tr>
                    <td><?= $user->name ?></td>
                    <td><?= $user->location ?></td>
                    <td><?= $user->team ?></td>
                    <td><?= $user->manager ?></td>
					<td><?= is_array($user->roles) ? implode(', ',$user->roles) : NULL ?></td>
					<td><?= is_array($user->applications) ? implode(', ',$user->applications) : NULL ?></td>
                    <td class="center">
                    	<?php $status_id = isset($user->status_id) ? $user->status_id : set_value("status_id"); ?>
						<?= form_dropdown('status_id', $statuses, $user->status_id, "id='{$user->user_id}' class='status_update'") ?>
                    </td>
                    <td class="center"><?= anchor("users/edit/".$user->user_id, 'Edit') ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
		<div class="pagination">
	        <?= $links ?>
	    </div>
    </div>
</div>