<?php if(validation_errors()):?>
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <?= validation_errors() ?>
	</div>
<?php endif;?>

<form class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="user_id" value="<?= isset($user->user_id) ? $user->user_id : set_value("user_id") ?>" />
	
	<div class="form-group">
		<label for="username" class="col-md-3 control-label">User Name: </label>
		<div class="col-md-5">
			<input type="text" name="username" class="input-xxlarge form-control" value="<?= isset($user->username) ? $user->username : set_value("username") ?>"/>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="first_name" class="col-md-3 control-label">First Name: </label>
		<div class="col-md-5">
			<input type="text" name="first_name" class="input-xxlarge form-control" value="<?= isset($user->first_name) ? $user->first_name : set_value("first_name") ?>"/>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="last_name" class="col-md-3 control-label">Last Name: </label>
		<div class="col-md-5">
			<input type="text" name="last_name" class="input-xxlarge form-control" value="<?= isset($user->last_name) ? $user->last_name : set_value("last_name") ?>"/>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="email" class="col-md-3 control-label">Email: </label>
		<div class="col-md-5">
			<input type="text" name="email" class="input-xxlarge form-control" value="<?= isset($user->email) ? $user->email : set_value("email") ?>"/>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="manager_id" class="col-md-3 control-label">Manager: </label>
		<div class="col-md-5">
			<?php $manager_id = isset($user->manager_id) ? $user->manager_id : set_value("manager_id"); ?>
			<?= form_dropdown('manager_id', $managers, $manager_id, 'class="form-control"') ?>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="location_id" class="col-md-3 control-label">Location: </label>
		<div class="col-md-5">
			<?php $location_id = isset($user->location_id) ? $user->location_id : set_value("location_id"); ?>
			<?= form_dropdown('location_id', $locations, $location_id, 'class="form-control"') ?>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="team_id" class="col-md-3 control-label">Team: </label>
		<div class="col-md-5">
			<?php $team_id = isset($user->team_id) ? $user->team_id : set_value("team_id"); ?>
			<?= form_dropdown('team_id', $teams, $team_id, 'class="form-control"') ?>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="status_id" class="col-md-3 control-label">Status: </label>
		<div class="col-md-5">
			<?php $status_id = isset($user->status_id) ? $user->status_id : set_value("status_id"); ?>
			<?= form_dropdown('status_id', $statuses, $status_id, 'class="form-control" id="status_id"') ?>
		</div>	
	</div>
	<div class="form-group status-date">
		<div class="input-daterange">
			<label for="status_begin_date" class="col-md-3 control-label">Leave Begins: </label>
			<div class="col-md-5">
				<div class="input-group date" id="datepicker-start" data-date="" data-date-format="yyyy-mm-dd">
				      <input name="status_begin_date" class="form-control" type="text" value="<?= isset($user->status_begin_date) ? $user->status_begin_date : set_value("status_begin_date") ?>">
				      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				    </div>			
			</div>	
		</div>
	</div>
	<div class="form-group status-date">
		<div class="input-daterange">
			<label for="status_end_date" class="col-md-3 control-label">Leave Ends: </label>
			<div class="col-md-5">
				<div class="input-group date" id="datepicker-end" data-date="" data-date-format="yyyy-mm-dd">
				      <input name="status_end_date" class="form-control" type="text" value="<?= isset($user->status_end_date) ? $user->status_end_date : set_value("status_end_date") ?>">
				      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				    </div>			
			</div>	
		</div>
	</div>
	<div class="form-group">
		<label class="col-xs-2 col-md-3 control-label">User Roles:</label>
		
		<div class="col-xs-3 col-md-5">

			<?php foreach ($roles as $role):?>
				<div class="checkbox">
					<label>
						<?php 
							if(isset($user->roles)):
								$checked = in_array($role->role_id, $user->roles) ? TRUE : FALSE;
							else:
								$checked = FALSE;
							endif; 
						?>
					 	<?= form_checkbox('role_id[]', $role->role_id, $checked) ?> <?= $role->role ?>
					</label>
				</div>
			<?php endforeach;?>
		</div>
	</div>
	
	<div class="form-group">
		<div class="input-daterange">
			<label for="start_date" class="col-md-3 control-label">Start Date: </label>
			<div class="col-md-5">
				<div class="input-group date" id="datepicker" data-date="" data-date-format="yyyy-mm-dd">
				      <input name="start_date" class="form-control" type="text" value="<?= isset($user->start_date) ? $user->start_date : set_value("start_date") ?>">
				      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				    </div>			
			</div>	
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-xs-2 col-md-3 control-label">Application Access:</label>
		
		<div class="col-xs-3 col-md-5">

			<?php foreach ($applications as $application):?>
				<div class="checkbox">
					<label>
						<?php 
							if(isset($user->applications)):
								$checked = in_array($application->application_id, $user->applications) ? TRUE : FALSE;
							else:
								$checked = FALSE;
							endif; 
						?>
					 	<?= form_checkbox('application_id[]', $application->application_id, $checked) ?> <?= $application->application ?>
					</label>
				</div>
			<?php endforeach;?>
		</div>
	</div>
	
	<div class="col-xs-3 col-sm-1 col-md-4 col-md-offset-4">
		<input class="btn btn-primary pull-right" type="submit" value="submit" />
	</div>
</form>