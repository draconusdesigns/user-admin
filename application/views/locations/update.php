<?php if(validation_errors()):?>
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <?= validation_errors() ?>
	</div>
<?php endif;?>

<form class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="location_id" value="<?= isset($location->location_id) ? $location->location_id : set_value("location_id") ?>" />
	
	<div class="form-group">
		<label for="location" class="col-md-3 control-label">Location Name: </label>
		<div class="col-md-5">
			<input type="text" name="location" class="input-xxlarge form-control" value="<?= isset($location->location) ? $location->location : set_value("location") ?>"/>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="description" class="col-md-3 control-label">Description: </label>
		<div class="col-md-5">
			<textarea class="text-left" rows="2" cols="54" type="text" name="description" id="description"><?= isset($location->description) ? $location->description : set_value("description") ?></textarea>
		</div>	
	</div>
	
	<div class="col-xs-3 col-sm-1 col-md-4 col-md-offset-4">
		<input class="btn btn-primary pull-right" type="submit" value="submit" />
	</div>
</form>