<div class="row">
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>locations/add" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa fa-plus-circle'></i> Add a New Location
		</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
        <table class="table table-striped tablesorter">
            <thead class="thead">
            <th>Location</th>
            <th>Location Description</th>
            <td class="center"><i class="fa fa-edit"></i></td>
            </thead>
            <tbody>
            <?php foreach ($locations as $location):?>
                <tr>
                    <td><?= $location->location ?></td>
                    <td><?= $location->description ?></td>
                    <td class="center"><?= anchor("locations/edit/".$location->location_id, 'Edit') ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div><!-- End #queue -->
</div><!-- End .row -->