<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
	'users/login' => array(
        array(
            'field' => 'identity',
            'label' => 'Username',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|xss_clean'
        ),
	),
    'user_validation' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|xss_clean|required|valid_email'
        ),
        array(
            'field' => 'manager_id',
            'label' => 'Manager',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'location_id',
            'label' => 'Location',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'team_id',
            'label' => 'Team',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'status_id',
            'label' => 'Status',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'start_date',
            'label' => 'Start Date',
            'rules' => 'trim|xss_clean|required'
        )
    ),
 	'role_validation' => array(
        array(
            'field' => 'role',
            'label' => 'Role',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|xss_clean'
        ),
    ),
 	'team_validation' => array(
        array(
            'field' => 'team',
            'label' => 'Team',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|xss_clean'
        ),
    ),
 	'location_validation' => array(
        array(
            'field' => 'location',
            'label' => 'Location',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|xss_clean'
        ),
    ),
 	'application_validation' => array(
        array(
            'field' => 'application',
            'label' => 'Application',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|xss_clean'
        ),
    )
);