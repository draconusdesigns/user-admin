<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Team_model extends CI_Model{
	
	public function __construct() {	
			
	}
    function getTeams(){
        $query = $this->db->get('teams');

        return $query->result();
    }

    function getTeam($team_id) {
        $query = $this->db->get_where('teams', array('team_id' => $team_id));
        return $query->row();
    }

    function addTeam($data){
        $query = array(
            'team' => $this->input->post('team'),
			'description' => $this->input->post('description')
        );

        $this->db->insert('teams', $query);
    }

    function updateTeam($data){	
	    $team_id = $this->input->post('team_id');
    
        $query = array(
            'team' => $this->input->post('team'),
			'description' => $this->input->post('description')
        );
		
        $this->db->where('team_id', $team_id);
        $this->db->update('teams', $query);
    }

	function getTeamDropdown() {
		$query = $this->db->order_by("team")->get('teams');		
		
		$data = array(' ' => '--Select a Team--');
		
		foreach ($query->result_array() as $row){
			$data[$row['team_id']] = $row['team'];
		}
		
		return $data;
	}
}