<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Status_model extends CI_Model{
	
	public function __construct() {	
			
	}
    function getStatuses(){
        $query = $this->db->get('statuses');

        return $query->result();
    }

    function getStatus($user_id) {
        $query = $this->db->get_where('statuses', array('status_id' => $status_id));
        return $query->row();
    }

    function addStatus($data){
        $query = array(
            'status' => $this->input->post('status')
        );

        $this->db->insert('statuses', $query);
    }

    function updateStatus($data){	
	    $status_id = $this->input->post('status_id');
    
        $query = array(
            'status' => $this->input->post('status')
        );
		
        $this->db->where('status_id', $status_id);
        $this->db->update('statuses', $query);
    }

	function getStatusDropdown() {
		$query = $this->db->order_by("status")->get('statuses');		
		
		$data = array(' ' => '--Select a Status--');
		
		foreach ($query->result_array() as $row){
			$data[$row['status_id']] = $row['status'];
		}
		
		return $data;
	}
}