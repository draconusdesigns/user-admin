<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{
	
	public function __construct() {	
			
	}
	
	function login(){
		$data = array(
            'identity' => $this->input->post('identity'),
        );

		$this->db->where('username', $data['identity']);
		$this->db->limit(1);
		$query = $this->db->get('users');

        if ($query->num_rows() > 0) {
            $row = $query->row();
     
            if ($row->status_id == 0) {
				$this->session->set_flashdata('error','Account not active');
                return false;
            }
				
            $data = array(
                'user_id' => $row->user_id,
                'username' => $row->username,
                'first_name' => $row->first_name,
                'last_name' => $row->last_name,
                'roles' => $this->getUserRoles($row->user_id),
                'applications' => $this->getUserApplications($row->user_id)
            );

        	$this->session->set_userdata($data);
			$this->session->set_flashdata('success','Logged In Successfully');
            return true;
        }
		$this->session->set_flashdata('error','User Not Found');
        return false;
    }

	function countUsers($user_search_terms=null){
		if (!empty($user_search_terms)){
			//map header field names to db field names and to themselves for recursive queries
			$db_fields = array(
				'name' => 'name',
				'location' => 'location',
				'team' => 'team',
				'manager' => 'manager',
				'status' => 'status'
			);
			
			foreach ( $user_search_terms as $term ) {
				$xS = explode(':', $term);
				$keys = explode(',', $xS[0]);
				$values = explode(',', $xS[1]);
				$all = null;
				// if the "all" keyword is used, get all columns from the view to iterate through
				if ( $keys[0] == 'all' ) {
					$new_keys = array();
					$sql = "SHOW COLUMNS FROM users_view";
					$query = $this->db->query($sql);
					if ( $query->num_rows() > 0 ) {
						foreach ( $query->result() as $result ) {
							$new_keys[] = $result->Field;
						}
					}
					$keys = $new_keys;
					$all = true;
				}
				//loop through search keys and values and add them to the query as like and or like statements
				$i=0;
				foreach ( $keys as $key ) {
					$key = str_replace('"', '', $key);
					foreach ( $values as $value ) {
						$key = $all ? strtolower($key) : $db_fields[strtolower($key)];
						if ( $i == 0 ) {
							$this->db->like($key, strtolower($value));
						} else {
							$this->db->or_like($key, strtolower($value));
						}
						$i++;
					}
				}
			}
		}else{
			$this->db->where('status','Active');
		}
		return $this->db->from('users_view')->count_all_results();
    }

    function getUserRoles($user_id) {
        $this->db->select('role_id');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user_roles');
        if ( $query->num_rows() > 0 ) {
            foreach ( $query->result() as $row ) {
                $data[] = $row->role_id;
            }
            return $data;
        }
        return null;
    }

	function getUserApplications($user_id) {
        $this->db->select('application_id');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user_applications');
        if ( $query->num_rows() > 0 ) {
            foreach ( $query->result() as $row ) {
                $data[] = $row->application_id;
            }
            return $data;
        }
        return null;
    }

    function getUsers(){
        $query = $this->db->get('users_view');
		
        return $query->result();
    }
	
	function getPaginatedUsers($limit, $offset = 0, $user_search_terms=null) {
		/* start search terms block */
		//if there are search terms, then loop through and set them to keys and values array
		if (!empty($user_search_terms)){
			//map header field names to db field names and to themselves for recursive queries
			$db_fields = array(
				'name' => 'name',
				'location' => 'location',
				'team' => 'team',
				'manager' => 'manager',
				'status' => 'status'
			);
			
			foreach ( $user_search_terms as $term ) {
				$xS = explode(':', $term);
				$keys = explode(',', $xS[0]);
				$values = explode(',', $xS[1]);
				$all = null;
				// if the "all" keyword is used, get all columns from the view to iterate through
				if ( $keys[0] == 'all' ) {
					$new_keys = array();
					$sql = "SHOW COLUMNS FROM users_view";
					$query = $this->db->query($sql);
					if ( $query->num_rows() > 0 ) {
						foreach ( $query->result() as $result ) {
							$new_keys[] = $result->Field;
						}
					}
					$keys = $new_keys;
					$all = true;
				}
				//loop through search keys and values and add them to the query as like and or like statements
				$i=0;
				foreach ( $keys as $key ) {
					$key = str_replace('"', '', $key);
					foreach ( $values as $value ) {
						$key = $all ? strtolower($key) : $db_fields[strtolower($key)];
						if ( $i == 0 ) {
							$this->db->like($key, strtolower($value));
						} else {
							$this->db->or_like($key, strtolower($value));
						}
						$i++;
					}
				}
			}
		}else{
			$this->db->where('status','Active');
		}
/* end search terms block */

		$this->db->order_by("name", "asc");
		
		$this->db->limit($limit, $offset);
		$query = $this->db->get("users_view");
		return $query->result();
	}
	
    function getUser($user_id) {	
        $query = $this->db->get_where('users', array('user_id' => $user_id));
        return $query->row();
    }

    function addUser($data){
		if($this->input->post('status_id') != 3):
			$status_begin_date = null;
			$status_end_date = null;
		else:
			$status_begin_date = $this->input->post('status_begin_date');
			$status_end_date = $this->input->post('status_end_date');
		endif;
		
        $query = array(
            'username' => $this->input->post('username'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'manager_id' => $this->input->post('manager_id'),
            'location_id' => $this->input->post('location_id'),
            'team_id' => $this->input->post('team_id'),
            'status_id' => $this->input->post('status_id'),
			'status_begin_date' => $status_begin_date,
			'status_end_date' => $status_end_date,
            'start_date' => $this->input->post('start_date'),
			'created' => date('Y-m-d H:i:s'),
			'created_by' => $this->session->userdata('user_id')
        );
        $query = $this->db->insert('users', $query);

		$user_id = $this->db->insert_id();
		$roles = $this->input->post('role_id');
		$applications = $this->input->post('application_id');
		
		if(!empty($roles)){
			//recurse roles and add to database for user
			foreach ($roles as $role_id){
				$this->db->set('user_id', $user_id);
				$this->db->set('role_id', $role_id);
		        $this->db->insert('user_roles');
			}
		}
		
		if(!empty($applications)){
			//recurse applications and add to database for user
			foreach ($applications as $application_id){
				$this->db->set('user_id', $user_id);
				$this->db->set('application_id', $application_id);
		        $this->db->insert('user_applications');
			}
		}
    }

    function updateUser($data){	
	    $user_id = $this->input->post('user_id');
	
		if($this->input->post('status_id') != 3):
			$status_begin_date = null;
			$status_end_date = null;
		else:
			$status_begin_date = $this->input->post('status_begin_date');
			$status_end_date = $this->input->post('status_end_date');
		endif; 
		
        $query = array(
            'username' => $this->input->post('username'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'manager_id' => $this->input->post('manager_id'),
            'location_id' => $this->input->post('location_id'),
            'team_id' => $this->input->post('team_id'),
            'status_id' => $this->input->post('status_id'),
			'status_begin_date' => $status_begin_date,
			'status_end_date' => $status_end_date,
            'start_date' => $this->input->post('start_date'), 
			'modified' => date('Y-m-d H:i:s'),
			'modified_by' => $this->session->userdata('user_id')
        );
		
        $this->db->where('user_id', $user_id);
        $this->db->update('users', $query);
		
		//remove all current roles for user to be reupdated below
		$this->db->where('user_id', $user_id);
        $this->db->delete('user_roles');
		
		$roles = $this->input->post('role_id');
		
		if(!empty($roles)){
			//recurse roles and add to database for user
			foreach ($roles as $role_id){
				$this->db->set('user_id', $user_id);
				$this->db->set('role_id', $role_id);
		        $this->db->insert('user_roles');
			}
		}
		
		//remove all current applications for user to be reupdated below
		$this->db->where('user_id', $user_id);
        $this->db->delete('user_applications');
		
		$applications = $this->input->post('application_id');
		
		if(!empty($applications)){
			//recurse applications and add to database for user
			foreach ($applications as $application_id){
				$this->db->set('user_id', $user_id);
				$this->db->set('application_id', $application_id);
		        $this->db->insert('user_applications');
			}
		}
    }
	
	function updateStatus($status_id, $user_id) {
		$this->db->where('user_id', $user_id);
		
		$query = array(
			'status_id' => $status_id
		);
		
		$this->db->update('users', $query);
	}
	
	function getManagerDropdown() {		
		$query = $this->db->get_where('users', array('status_id' => '1'));		
		
		$data = array(' ' => '--Select a Manager--');
		
		foreach ($query->result_array() as $row){
			$data[$row['user_id']] = $row['first_name'] . ' ' . $row['last_name'] ;
		}
		
		return $data;
	}
}