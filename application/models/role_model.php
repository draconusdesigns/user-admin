<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* I must try my best to be a good role model ;) */
class Role_model extends CI_Model {

    public function getRoles() {
        $query = $this->db->get('roles');
        return $query->result();
    }

	public function getRole($role_id) {
		$query = $this->db->get_where('roles', array('role_id' => $role_id));		
		return $query->row();
	}

	public function addRole($data) {	
		$query = array(
			'role' => $this->input->post('role'),
			'description' => $this->input->post('description')
		);
				
		$this->db->insert('roles', $query);		
	}

    public function editRole($data) {
		$role_id = $this->input->post('role_id');
		
        $data = array(
			'role' => $this->input->post('role'),
			'description' => $this->input->post('description')
        );
        $update = $this->db->where(array('role_id'=>$role_id))->update('roles', $data);
        return $update ? true : false;
    }

    public function add_user_to_role($user_id, $role_id) {
        $data = array(
            'user_id' => $user_id,
            'role_id'=> $role_id
        );
        $insert = $this->db->insert('user_roles', $data);
        return $insert ? true : false;
    }

    public function remove_user_from_role($user_id, $role_id) {
        $where = array(
            'user_id' => $user_id,
            'role_id' => $role_id
        );
        $delete = $this->db->where($where)->delete('user_roles');
        return $delete ? true : false;
    }
	function getUserRolesArray($user_id){
		$this->db->join('roles', 'roles.role_id = user_roles.role_id');
		
		$query = $this->db->get_where('user_roles', array('user_id' => $user_id));	
		
		$data = null;
		foreach ($query->result_array() as $row){
			$data[] = $row['role'];
		}
		
		return $data;	
	}
	function getUserRolesIDArray($user_id){
		$this->db->join('roles', 'roles.role_id = user_roles.role_id');
		
		$query = $this->db->get_where('user_roles', array('user_id' => $user_id));	
		$data = null;
		foreach ($query->result_array() as $row){
			$data[] = $row['role_id'];
		}
		
		return $data;	
	}
}