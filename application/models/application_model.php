<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Application_model extends CI_Model{
	
	public function __construct() {	
			
	}
    function getApplications(){
        $query = $this->db->get('applications');

        return $query->result();
    }

    function getApplication($application_id) {
        $query = $this->db->get_where('applications', array('application_id' => $application_id));
        return $query->row();
    }

    function addApplication($data){
        $query = array(
            'application' => $this->input->post('application'),
			'description' => $this->input->post('description')
        );

        $this->db->insert('applications', $query);
    }

    function updateApplication($data){	
	    $application_id = $this->input->post('application_id');
    
        $query = array(
            'application' => $this->input->post('application'),
			'description' => $this->input->post('description')
        );
		
        $this->db->where('application_id', $application_id);
        $this->db->update('applications', $query);
    }

	function getApplicationDropdown() {
		$query = $this->db->get('applications');		
		
		$data = array(' ' => '--Select a Application--');
		
		foreach ($query->result_array() as $row){
			$data[$row['application_id']] = $row['application'];
		}
		
		return $data;
	}
	
	function getUserApplicationsArray($user_id){
		$this->db->join('applications', 'applications.application_id = user_applications.application_id');
		
		$query = $this->db->get_where('user_applications', array('user_id' => $user_id));	
		
		$data = null;
		foreach ($query->result_array() as $row){
			$data[] = $row['application'];
		}
		
		return $data;	
	}
	function getUserApplicationIDArray($user_id){
		$this->db->join('applications', 'applications.application_id = user_applications.application_id');
		
		$query = $this->db->get_where('user_applications', array('user_id' => $user_id));	
		$data = null;
		foreach ($query->result_array() as $row){
			$data[] = $row['application_id'];
		}
		
		return $data;	
	}
}