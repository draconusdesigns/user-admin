<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Location_model extends CI_Model{
	
	public function __construct() {	
			
	}
    function getLocations(){
        $query = $this->db->get('locations');

        return $query->result();
    }

    function getLocation($location_id) {
        $query = $this->db->get_where('locations', array('location_id' => $location_id));
        return $query->row();
    }

    function addLocation($data){
        $query = array(
            'location' => $this->input->post('location'),
			'description' => $this->input->post('description')
        );

        $this->db->insert('locations', $query);
    }

    function updateLocation($data){	
	    $location_id = $this->input->post('location_id');
    
        $query = array(
            'location' => $this->input->post('location'),
			'description' => $this->input->post('description')
        );
		
        $this->db->where('location_id', $location_id);
        $this->db->update('locations', $query);
    }

	function getLocationDropdown() {
		$query = $this->db->order_by("location")->get('locations');		
		
		$data = array(' ' => '--Select a Location--');
		
		foreach ($query->result_array() as $row){
			$data[$row['location_id']] = $row['location'];
		}
		
		return $data;
	}
}