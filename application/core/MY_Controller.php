<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

    function __construct(){
		parent::__construct();
		
		header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
		header("Expires: Sat, 08 Jul 1997 05:00:00 GMT"); // Date in the past
		session_cache_limiter(0);

		$user['is_admin'] = $this->is_of_role(1);
		$user['is_manager'] = $this->is_of_role(2);
		$user['is_ae'] = $this->is_of_role(3);
		$user['is_design'] = $this->is_of_role(4);
		
		$user['lexicon'] = $this->has_app_access(1);
				
		$this->load->vars($user);

		

        $this->output->enable_profiler(TRUE);
    }

    function is_logged_in(){
        $this->session->userdata('user_id') || redirect('users/login', 'refresh');
    }

	function is_of_role($roles) {		
        if ( !$this->session->userdata('roles') ) {
            return false;
        }
        $user_roles = $this->session->userdata('roles');
        if ( is_array($roles) ) {
            foreach ( $roles as $role ) {
                if ( in_array($role, $user_roles) ) {
					$this->data['role'] = $roles;
                    return true;
                }
            }
        } else {
            if ( in_array($roles, $user_roles) ) {	
				$this->data['role'] = array($roles);
                return true;
            }
        }
        return false;
    }

	function has_application_access($application) {		
        if ( !$this->session->userdata('applications') ) {
	 		$this->session->set_flashdata('error','User Account does not have access to this application');
            return false;
        }
        $user_applications = $this->session->userdata('applications');

		if ( in_array($application, $user_applications) ) {	
			return true;
		}
		$this->session->set_flashdata('error','User Account does not have access to this application');
        return false;
    }

	//The same as above but without error flags...
	function has_app_access($application) {		
        if ( !$this->session->userdata('applications') ) {
            return false;
        }
        $user_applications = $this->session->userdata('applications');

		if ( in_array($application, $user_applications) ) {	
			return true;
		}
        return false;
    }

	function json($arr) {
		$this->output->enable_profiler(false);
		echo json_encode($arr);
	}
}