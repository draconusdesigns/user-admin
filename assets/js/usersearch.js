$(document).ready(function(e){
/* Dynamic Search Functions */		
	var sBtn = $('#search-btn'),
	    sTrm = $('#search-terms'),
	    loader = $('#loader-icon'),
	    statuses = ['<?= implode("', '", $statuses); ?>'],
		rTerm = $('button.rem-term');
	rTerm.click(function() {
		if ( rTerm.length-1 == 0 ) {
			window.location = base_url + 'users/search_reset';
			return;
		} else {
			var term = $(this).data('rel');
			$.post(
				base_url + 'users/remove_term',
				{
					term: term
				},
				function(data) {
					if ( data['response'].length > 0 ) {
						if ( data['response'] == 'success' ) {
							window.location.reload();
						}
					}
				}, 'json'
			);
		}
	});
	sTrm.val('').focus();
	sBtn.click(function(e) {
		var term = sTrm.val(),
	        keys = null,
			vals = null;  
		if(sTrm.val() !=''){
			sBtn.hide();
		    loader.show();
		    e.preventDefault();
	    if (term.indexOf(':') !== -1) {
			xS = term.split(':');
	        keys = xS[0];
			vals = xS[1];
	        if (keys.indexOf(',') !== -1) {
	            keys = keys.split(',');
	            var n_keys = [];
	            for (var i = 0; i < keys.length; i++) {
	                var key = $.inArray(keys[i].toLowerCase().trim(), options(1)) < 0 ? 'All' : keys[i];
	                make_filter(key, term.split(':')[1].replace(/\W+,/g, ''));
	                n_keys.push(key);
	            }
	            keys = n_keys;
	        } else {
	            keys = $.inArray(keys.toLowerCase().trim(), options(1)) < 0 ? 'All' : keys.trim();
	            make_filter(keys, term.split(':')[1].replace(/\W+,/g, ''));
	        }
	    } else {
	        keys = $('#search-term').val();
	        make_filter(keys, term);
			vals = term;
	    }
		term = keys + ':' + vals;
		    $.post(
		        base_url + 'users/search', {
		            terms: term
		        },
		        function(data) {
					if ( data['response'].length > 0 ) {
						if ( data['response'] == 'success' ) {
							window.location.reload();
						}
					}
		        }, 'json'
		    );
		}else{
			alert('Search Field Cannot be Empty');
			sTrm.focus();
		}
	});
	addOptions();
});

/* Associated Functions */		

function make_filter(lbl_1, lbl_2) {
    var str = '';
    str += "<div class='alert alert-info alert-dismissible filter pull-left' role='alert'>";
    str += "<button type='button' class='close' data-dismiss='alert'>";
    str += "<span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span>";
    str += "</button>";
    str += "<strong>" + ucwords(lbl_1) + " : </strong>" + lbl_2;
    str += "</div>";
    $(str).insertBefore("#search-results");
}

function ucwords(str) {
    return (str + '')
        .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
            return $1.toUpperCase();
        });
}

function addOptions() {
    var sOpt = $('#search-term'),
        heads = options(0);
    $.each(heads, function(k, v) {
        $('<option value="' + v + '">' + v + '</option>').appendTo(sOpt);
    });
}

function options(lower) {
    var heads = [];
    $('#search-results').find('th').each(function() {
        if ($(this).text() != '') {
            if (lower == 1) {
                heads.push($(this).text().toLowerCase());
            } else {
                heads.push($(this).text());
            }
        }
    });
    return heads;
}