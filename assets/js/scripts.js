$(document).ready(function() {
	$("table.tablesorter").tablesorter();
	
	$('#datepicker').datepicker({
	    format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true
	})
	
	$('#datepicker-start').datepicker({
	    format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true
	})
	
	$('#datepicker-end').datepicker({
	    format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true
	})
	
	$('#datepicker_hit_date').datepicker({
	    format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true
	})
	
	$('#datepicker_release_date').datepicker({
	    format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true
	})

	
	$('#weekend').datepicker({
		format: "yyyy-mm-dd",
		daysOfWeekDisabled: "0,1,2,3,4,5",
		autoclose: true,
		todayHighlight: true
	})
/* Show and Hide User Status Dates in User Update*/
	if($('#status_id option:selected').val() != 3){
		$('.status-date').hide();
	}

	$('#status_id').change(function () { 
	
		if($('#status_id option:selected').val() == 3){
			$('.status-date').show();
		}else{
			$('.status-date').hide();
		}
	});
		
/* Update User Status in User Index */	

	$(".status_update").change(function() {
		var status_id = $(this).val();
		var job_id =  $(this).attr('id');

		update_status(status_id, job_id);
	});
});
	
function update_status(status_id, job_id) {    
	$.ajax({
    	type: "POST",
        url: base_url + "users/update_status/" + status_id + "/" + job_id
	});
}